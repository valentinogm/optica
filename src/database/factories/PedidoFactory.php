<?php

namespace Database\Factories;

use App\Models\Pedido;
use Illuminate\Database\Eloquent\Factories\Factory;

class PedidoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pedido::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Obtener IDs aleatorios de modelos relacionados
        $vendedorId = \App\Models\User::inRandomOrder()->first()->id;
        $clienteId = \App\Models\Cliente::inRandomOrder()->first()->id;
        $oftalmologoId = \App\Models\Oftalmologo::inRandomOrder()->first()->id;
        $laboratorioId = \App\Models\Laboratorio::inRandomOrder()->first()->id;
        $cercaLenteId = \App\Models\CercaLente::inRandomOrder()->first()->id;
        $lejosLenteId = \App\Models\LejosLente::inRandomOrder()->first()->id;
        $obraSocialId = \App\Models\ObraSocial::inRandomOrder()->first()->id;

        return [
            'codigo' => $this->faker->unique()->regexify('[A-Z0-9]{8}'), // Genera un código único
            'vendedor_id' => $vendedorId,
            'cliente_id' => $clienteId,
            'oftalmologo_id' => $oftalmologoId,
            'laboratorio_id' => $laboratorioId,
            'cerca_lente_id' => $cercaLenteId,
            'lejos_lente_id' => $lejosLenteId,
            'obra_social_id' => $obraSocialId,
            'fecha_pedido' => $this->faker->date,
            'fecha_estimada' => $this->faker->date,
            'fecha_entrega' => $this->faker->optional()->date, // Fecha de entrega opcional
            'importe' => $this->faker->numberBetween(100, 1000), // Importe aleatorio entre 100 y 1000
            'senia' => $this->faker->numberBetween(0, 500), // Seña aleatoria entre 0 y 500
            'saldo' => $this->faker->numberBetween(0, 500), // Saldo aleatorio entre 0 y 500
            'tipo' => $this->faker->randomElement(['tipo1', 'tipo2', null]), // Tipo aleatorio o nulo
            'observaciones' => $this->faker->optional()->text, // Observaciones opcionales
        ];
    }
}
