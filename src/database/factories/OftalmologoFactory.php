<?php

namespace Database\Factories;

use App\Models\Oftalmologo;
use Illuminate\Database\Eloquent\Factories\Factory;

class OftalmologoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Oftalmologo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'matricula' => $this->faker->unique()->randomNumber(8),
            'nombre' => $this->faker->firstName,
            'apellido' => $this->faker->lastName,
        ];
    }
}
