<?php

namespace Database\Factories;

use App\Models\Cristal;
use Illuminate\Database\Eloquent\Factories\Factory;

class CristalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cristal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->word, // Genera un nombre aleatorio
        ];
    }
}
