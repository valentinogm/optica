<?php

namespace Database\Factories;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cliente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'dni' => $this->faker->unique()->regexify('[0-9]{8}[A-Z]{1}'), // Genera un DNI aleatorio con un dígito verificador
            'nombre' => $this->faker->firstName,
            'apellido' => $this->faker->lastName,
            'domicilio' => $this->faker->streetAddress,
            'localidad' => $this->faker->city,
            'fecha_nac' => $this->faker->date,
            'telefono' => $this->faker->phoneNumber,
            'mail' => $this->faker->unique()->safeEmail, // Genera un correo electrónico aleatorio único
        ];
    }
}

