<?php

namespace Database\Factories;

use App\Models\TipoArmazon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TipoArmazonFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TipoArmazon::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tipo_material' => $this->faker->word, // Genera un tipo de material aleatorio
            'nombre' => $this->faker->word, // Genera un nombre aleatorio
        ];
    }
}
