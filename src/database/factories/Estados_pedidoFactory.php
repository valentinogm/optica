<?php

namespace Database\Factories;

namespace Database\Factories;

use App\Models\Estados_pedido;
use Illuminate\Database\Eloquent\Factories\Factory;

class Estados_pedidoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Estados_pedido::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Obtener IDs aleatorios de modelos relacionados
        $pedidoId = \App\Models\Pedido::inRandomOrder()->first()->id;
        $estadoId = \App\Models\Estado::inRandomOrder()->first()->id;

        return [
            'id_pedido' => $pedidoId,
            'id_estado' => $estadoId,
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (Estados_pedido $estadosPedido) {
            // Generar un número aleatorio de estados para cada pedido (entre 1 y 5)
            $numEstados = rand(1, 5);
            $pedidoId = $estadosPedido->id_pedido;

            // Crear instancias adicionales de Estados_pedido para este pedido
            for ($i = 1; $i < $numEstados; $i++) {
                Estados_pedido::factory()->create(['id_pedido' => $pedidoId]);
            }
        });
    }
}
