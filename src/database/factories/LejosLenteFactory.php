<?php

namespace Database\Factories;

use App\Models\LejosLente;
use Illuminate\Database\Eloquent\Factories\Factory;

class LejosLenteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LejosLente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Obtener IDs aleatorios de TipoArmazon y Cristal
        $tipoArmazonId = \App\Models\TipoArmazon::inRandomOrder()->first()->id;
        $cristalId = \App\Models\Cristal::inRandomOrder()->first()->id;
        $tipoLenteId = \App\Models\TipoLente::inRandomOrder()->first()->id;
        return [
            'tipo_armazon_id' => $tipoArmazonId,
            'cristal_id' => $cristalId,
            'tipo_lente_id' => $tipoLenteId,
        ];
    }
}
