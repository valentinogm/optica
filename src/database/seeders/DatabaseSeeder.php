<?php

namespace Database\Seeders;
use App\Models\Pedido;
use App\Models\CercaLente;
use App\Models\LejosLente;
use App\Models\Estados_pedido;
use App\Models\TipoArmazon;
use App\Models\Oftalmologo;
use App\Models\Laboratorio;
use App\Models\Cristal;
use App\Models\Cliente;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();
        $this->call(ObrasSocialesSeeder::class);
        $this->call(EstadosSeeder::class);
        $this->call(RoleSeeder::class);
        //$this->call(TipoLentesSeeder::class);
        //Cliente::factory()->count(100)->create();
        //Oftalmologo::factory()->count(100)->create();
        //TipoArmazon::factory()->count(150)->create();
        //Laboratorio::factory()->count(50)->create();
        //Cristal::factory()->count(300)->create();
        //CercaLente::factory()->count(200)->create();
        //LejosLente::factory()->count(200)->create();
        //Pedido::factory()->count(100)->create();
        //Estados_pedido::factory()->count(500)->create();
    }
}
