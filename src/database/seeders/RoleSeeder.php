<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permisos = [
            //obra social
            'ObraSocial.index',
            'ObraSocial.create',
            'ObraSocial.edit',
            'ObraSocial.destroy',
            //clientes
            'Clientes.index',
            'Clientes.create',
            'Clientes.edit',
            'Clientes.destroy',
            'Clientes.show',
            //Oftalmologo
            'Oftalmologo.index',
            'Oftalmologo.create',
            'Oftalmologo.edit',
            'Oftalmologo.destroy',
            //Laboratorio
            'Laboratorio.index',
            'Laboratorio.create',
            'Laboratorio.edit',
            'Laboratorio.destroy',
            //tipo_armazon
            'tipo_armazon.index',
            'tipo_armazon.create',
            'tipo_armazon.edit',
            'tipo_armazon.destroy',
            //cristal
            'cristal.index',
            'cristal.create',
            'cristal.edit',
            'cristal.destroy',
            //pedido
            'pedido.index',
            'pedido.create',
            'pedido.edit',
            'pedido.destroy',
            'pedido.show',
            'pedido.clientesobtener',
            'pedido.verestados',
            'pedido.actualizarestado',
            //vendedor
            'vendedor.index',
            'vendedor.create',
            'vendedor.edit',
            'vendedor.destroy',
            'vendedor.show',
            'index',
        ];
        foreach ($permisos as $permiso) {
            Permission::create(['name' => $permiso]);
        }

        $role1 = Role::create(['name' => 'Admin']);
        $role1->syncPermissions($permisos);
        $adminUser = User::create([
            'name' => 'Admin',
            'apellido' => 'Admin',
            'email' => 'admin@optica.com',
            'password' => Hash::make('admin123'), 
            'documento' => 7777777
        ]);
        $adminUser->assignRole('Admin');

        $role2 = Role::create(['name' => 'Vendedor']);
        $permisosVendedor = [
            //obra social
            'ObraSocial.index',
            'ObraSocial.create',
            'ObraSocial.edit',
            //clientes
            'Clientes.index',
            'Clientes.create',
            'Clientes.edit',
            'Clientes.show',
            //Oftalmologo
            'Oftalmologo.index',
            'Oftalmologo.create',
            'Oftalmologo.edit',
            //Laboratorio
            'Laboratorio.index',
            'Laboratorio.create',
            'Laboratorio.edit',
            //tipo_armazon
            'tipo_armazon.index',
            'tipo_armazon.create',
            'tipo_armazon.edit',
            //cristal
            'cristal.index',
            'cristal.create',
            'cristal.edit',
            //pedido
            'pedido.index',
            'pedido.create',
            'pedido.edit',
            'pedido.show',
            'pedido.verestados',
            'pedido.actualizarestado',
            'index',
        ];
        $role2->syncPermissions($permisosVendedor);
    }
}
