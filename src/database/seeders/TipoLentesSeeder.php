<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TipoLente;

class TipoLentesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tipoLentes = [
            'Bifocales',
            'Multifocales',
            'Ocupacional',
            'Monofocal'
        ];
        
        foreach($tipoLentes as $tipoLente){
            TipoLente::create(['nombre' => $tipoLente]);
        }
    }
}
