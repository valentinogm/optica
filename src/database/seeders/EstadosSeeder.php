<?php

namespace Database\Seeders;

use App\Models\Estado;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use PhpParser\Node\Stmt\Foreach_;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $estados = [
            'EN PROCESO',
            'EN LABORATORIO',
            'PASÓ CONTROL DE CALIDAD',
            'TERMINADO',
        ];

        foreach($estados as $estado){
            Estado::create(['desc' => $estado]);
        }
    }
}
