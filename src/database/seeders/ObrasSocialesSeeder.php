<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ObraSocial;

class ObrasSocialesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $obrasSociales = [
            'PAMI',
            'PODER JUDICIAL',
            'UPCN',
            'IOSPER',
            '10 DE ABRIL',
            'MUPER',
            'MUTUAL URQUIZA',
            'ASE',
            'BANCARIOS',
            'MEDIFE',
            'SANCOR',
            'JERARQUICOS SALUD',
            'OSDE',
            'IOSE',
            'OSPECON/UOCRA',
            'PARTICULAR',
        ];

        foreach ($obrasSociales as $obraSocial) {
            ObraSocial::create(['nombre' => $obraSocial]);
        }
    }
}
