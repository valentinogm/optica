<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('oftalmologo', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('matricula');
            $table->string('nombre');
            $table->string('apellido');
            $table->softDeletes();
            $table->unsignedInteger('deleted_by_user_id')->nullable()->after('deleted_at');
            $table->foreign('deleted_by_user_id')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('oftalmologo');
    }
};
