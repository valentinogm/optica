<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cerca_lente', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedInteger('tipo_armazon_id');
            $table->unsignedInteger('cristal_id');
            $table->foreign('tipo_armazon_id')->references('id')->on('tipo_armazon');
            $table->foreign('cristal_id')->references('id')->on('cristal');
            $table->softDeletes();
            $table->unsignedInteger('deleted_by_user_id')->nullable()->after('deleted_at');
            $table->foreign('deleted_by_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cerca_lente');
    }
};
