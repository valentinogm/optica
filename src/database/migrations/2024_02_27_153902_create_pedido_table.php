<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pedido', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('codigo')->unique()->nullable();
            $table->unsignedInteger('vendedor_id');
            $table->unsignedInteger('cliente_id');
            $table->unsignedInteger('oftalmologo_id');
            $table->unsignedInteger('laboratorio_id');
            $table->unsignedBigInteger('cerca_lente_id')->nullable();
            $table->unsignedBigInteger('lejos_lente_id')->nullable();
            $table->unsignedInteger('obra_social_id');
            $table->date('fecha_pedido');
            $table->date('fecha_estimada');
            $table->date('fecha_entrega')->nullable();
            $table->integer('importe');
            $table->integer('senia');
            $table->integer('saldo');
            $table->string('tipo')->nullable();
            $table->string('observaciones')->nullable();
            $table->string('receta')->nullable();
            $table->foreign('vendedor_id')->references('id')->on('users');
            $table->foreign('cliente_id')->references('id')->on('cliente');
            $table->foreign('oftalmologo_id')->references('id')->on('oftalmologo');
            $table->foreign('laboratorio_id')->references('id')->on('laboratorio');
            $table->foreign('cerca_lente_id')->references('id')->on('cerca_lente');
            $table->foreign('lejos_lente_id')->references('id')->on('lejos_lente');
            $table->foreign('obra_social_id')->references('id')->on('obra_social');
            $table->softDeletes();
            $table->unsignedInteger('deleted_by_user_id')->nullable()->after('deleted_at');
            $table->foreign('deleted_by_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pedido');
    }
};
