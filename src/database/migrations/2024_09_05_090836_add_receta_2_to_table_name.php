<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReceta2ToTableName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedido', function (Blueprint $table) {
            // Añade el nuevo campo receta_2 como un string y lo configura como nullable
            $table->string('receta_2')->nullable()->after('receta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedido', function (Blueprint $table) {
            // Elimina el campo receta_2 en caso de que se revierta la migración
            $table->dropColumn('receta_2');
        });
    }
}
