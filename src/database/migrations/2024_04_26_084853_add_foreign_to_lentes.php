<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('cerca_lente', function (Blueprint $table) {
            $table->unsignedBigInteger('tipo_lente_id');
            $table->foreign('tipo_lente_id')->references('id')->on('tipo_lente');
        });

        Schema::table('lejos_lente', function (Blueprint $table) {
            $table->unsignedBigInteger('tipo_lente_id');
            $table->foreign('tipo_lente_id')->references('id')->on('tipo_lente');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('cerca_lente', function (Blueprint $table) {
            $table->dropForeign(['tipo_lente_id']);
            $table->dropColumn('tipo_lente_id');
        });
    
        Schema::table('lejos_lente', function (Blueprint $table) {
            $table->dropForeign(['tipo_lente_id']);
            $table->dropColumn('tipo_lente_id');
        });
    }
};
