<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BackupController;
use App\Http\Controllers\PedidoController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\CristalController;
use App\Http\Controllers\VendedorController;
use App\Http\Controllers\ObraSocialController;
use App\Http\Controllers\LaboratorioController;
use App\Http\Controllers\OftalmologoController;
use App\Http\Controllers\TipoArmazonController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[HomeController::class,'index']);

Auth::routes();
Route::get('backups', [BackupController::class, 'index'])->name('backups.index');
Route::post('backups/create', [BackupController::class, 'create'])->name('backups.create');
Route::get('backups/download/{file_name}', [BackupController::class, 'download'])->name('backups.download');
Route::delete('backups/delete/{file_name}', [BackupController::class, 'delete'])->name('backups.delete');
Route::get('/backup/clean', [BackupController::class, 'cleanBackup'])->name('backups.clean');

//---------------------------------------------------------------------------------------------------
Route::get('reports', [ReportController::class, 'index'])->name('reports.index');
Route::post('reports/generate', [ReportController::class, 'generateReport'])->name('reports.generate');

Route::prefix('ObraSocial')->group(function () {
    Route::get('/', [ObraSocialController::class, 'index'])->name('ObraSocial.index');
    Route::get('/create', [ObraSocialController::class, 'create'])->name('ObraSocial.create');
    Route::post('/', [ObraSocialController::class, 'store'])->name('ObraSocial.store');
    Route::get('/{id}/edit', [ObraSocialController::class, 'edit'])->name('ObraSocial.edit');
    Route::put('/{id}', [ObraSocialController::class, 'update'])->name('ObraSocial.update');
    Route::delete('/{id}', [ObraSocialController::class, 'destroy'])->name('ObraSocial.destroy');
    Route::get('/obras', [ObraSocialController::class, 'obtenerObras'])->name('pedido.obrasobtener');
});

Route::prefix('Clientes')->group(function(){
    Route::get('/', [ClienteController::class, 'index'])->name('Clientes.index');
    Route::get('/create', [ClienteController::class, 'create'])->name('Clientes.create');
    Route::post('/', [ClienteController::class, 'store'])->name('Clientes.store');
    Route::get('/{id}/edit', [ClienteController::class, 'edit'])->name('Clientes.edit');
    Route::put('/{id}', [ClienteController::class, 'update'])->name('Clientes.update');
    Route::delete('/{id}', [ClienteController::class, 'destroy'])->name('Clientes.destroy');
    Route::get('/{id}/show', [ClienteController::class, 'show'])->name('Clientes.show');
});


Route::prefix('Oftalmologo')->group(function () {
    Route::get('/', [OftalmologoController::class, 'index'])->name('Oftalmologo.index');
    Route::get('/create', [OftalmologoController::class, 'create'])->name('Oftalmologo.create');
    Route::post('/', [OftalmologoController::class, 'store'])->name('Oftalmologo.store');
    Route::get('/{id}/edit', [OftalmologoController::class, 'edit'])->name('Oftalmologo.edit');
    Route::put('/{id}', [OftalmologoController::class, 'update'])->name('Oftalmologo.update');
    Route::delete('/{id}', [OftalmologoController::class, 'destroy'])->name('Oftalmologo.destroy');
    Route::get('/oftalmologos', [OftalmologoController::class, 'obtenerOftalmologos'])->name('pedido.oftalmologosobtener');
});

Route::prefix('Laboratorio')->group(function () {
    Route::get('/', [LaboratorioController::class, 'index'])->name('Laboratorio.index');
    Route::get('/create', [LaboratorioController::class, 'create'])->name('Laboratorio.create');
    Route::post('/', [LaboratorioController::class, 'store'])->name('Laboratorio.store');
    Route::get('/{id}/edit', [LaboratorioController::class, 'edit'])->name('Laboratorio.edit');
    Route::put('/{id}', [LaboratorioController::class, 'update'])->name('Laboratorio.update');
    Route::delete('/{id}', [LaboratorioController::class, 'destroy'])->name('Laboratorio.destroy');
    Route::get('/laboratorios', [LaboratorioController::class, 'obtenerOLaboratorios'])->name('pedido.laboratoriosobtener');
});

Route::prefix('tipo_armazon')->group(function () {
    Route::get('/', [TipoArmazonController::class, 'index'])->name('tipo_armazon.index');
    Route::get('/create', [TipoArmazonController::class, 'create'])->name('tipo_armazon.create');
    Route::post('/', [TipoArmazonController::class, 'store'])->name('tipo_armazon.store');
    Route::get('/{id}/edit', [TipoArmazonController::class, 'edit'])->name('tipo_armazon.edit');
    Route::put('/{id}', [TipoArmazonController::class, 'update'])->name('tipo_armazon.update');
    Route::delete('/{id}', [TipoArmazonController::class, 'destroy'])->name('tipo_armazon.destroy');
});

Route::prefix('cristal')->group(function () {
    Route::get('/', [CristalController::class, 'index'])->name('cristal.index');
    Route::get('/create', [CristalController::class, 'create'])->name('cristal.create');
    Route::post('/', [CristalController::class, 'store'])->name('cristal.store');
    Route::get('/{id}/edit', [CristalController::class, 'edit'])->name('cristal.edit');
    Route::put('/{id}', [CristalController::class, 'update'])->name('cristal.update');
    Route::delete('/{id}', [CristalController::class, 'destroy'])->name('cristal.destroy');
});

Route::prefix('pedido')->group(function () {
    Route::get('/', [PedidoController::class, 'index'])->name('pedido.index');
    Route::get('/create', [PedidoController::class, 'create'])->name('pedido.create');
    Route::post('/', [PedidoController::class, 'store'])->name('pedido.store');
    Route::get('/{id}/created', [PedidoController::class, 'created'])->name('pedido.created');
    Route::get('/{id}/edit', [PedidoController::class, 'edit'])->name('pedido.edit');
    Route::put('/{id}', [PedidoController::class, 'update'])->name('pedido.update');
    Route::delete('/{id}', [PedidoController::class, 'destroy'])->name('pedido.destroy');
    Route::get('/{id}/show', [PedidoController::class, 'show'])->name('pedido.show');
    Route::get('/clientes', [PedidoController::class, 'obtenerClientes'])->name('pedido.clientesobtener');
    Route::get('/{id}/estados', [PedidoController::class, 'verEstados'])->name('pedido.verestados');
    Route::get('/{id_pedido}/actualizarEstado/{id_estado}', [PedidoController::class, 'actualizarEstado'])->name('pedido.actualizarestado');
    Route::get('/{id}/lab', [PedidoController::class, 'lab'])->name('pedido.lab');
    Route::get('/pdf', [PedidoController::class, 'generatePdf'])->name('pedido.pdf');
    Route::get('/excel', [PedidoController::class, 'generateExcel'])->name('pedido.excel');
});

Route::prefix('vendedor')->group(function(){
    Route::get('/',[VendedorController::class, 'index'])->name('vendedor.index');
    Route::get('/create',[VendedorController::class, 'create'])->name('vendedor.create');
    Route::post('/',[VendedorController::class, 'store'])->name('vendedor.store');
    Route::get('/{id}/edit',[VendedorController::class, 'edit'])->name('vendedor.edit');
    Route::put('/{id}',[VendedorController::class, 'update'])->name('vendedor.update');
    Route::delete('/{id}', [VendedorController::class, 'destroy'])->name('vendedor.destroy');
    Route::get('/{id}/show', [VendedorController::class, 'show'])->name('vendedor.show');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
