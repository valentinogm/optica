@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="page-header">
                    <br>
                    <hr>
                    <div class="d-flex flex-wrap justify-content-end">
                        <a class="btn btn-success mb-2" href="{{ route('ObraSocial.create') }}">Nueva&nbsp;&nbsp;<span class="fa fa-plus"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Gestión de Obras Sociales</div>
                    <div class="card-body">
                        <form action="{{ route('ObraSocial.index') }}" method="GET" class="form-inline mb-3">
                            <input class="form-control mr-sm-2 mb-2" type="search" placeholder="Buscar por nombre" aria-label="Buscar" name="q" value="{{ request('q') }}">
                            <button class="btn btn-outline-primary my-2 my-sm-0 mb-2" type="submit">Buscar</button>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th class="text-right">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($obrasSociales as $obraSocial)
                                        <tr>
                                            <td>{{ $obraSocial->id }}</td>
                                            <td>{{ $obraSocial->nombre }}</td>
                                            <td class="text-right">
                                                <div class="d-flex justify-content-end">
                                                    <a href="{{ route('ObraSocial.edit', $obraSocial->id) }}" class="btn btn-primary mr-2">Editar</a>
                                                    @can('ObraSocial.destroy')
                                                        <form action="{{ route('ObraSocial.destroy', $obraSocial->id) }}" class="d-inline formulario-eliminar" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-danger">Borrar</button>
                                                        </form>
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="float-right">
        {{ $obrasSociales->links() }}
    </div>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    @if (session('success') == 'ok')
    <script>
        Swal.fire({
            position: "top-end",
            icon: "success",
            title: "Se guardó con éxito",
            showConfirmButton: false,
            timer: 1500
        });
    </script>
    @endif

    @if (session('eliminar')  == 'ok')
    <script>
        Swal.fire({
            title: "Eliminado!",
            text: "Se eliminó correctamente.",
            icon: "success"
        });
    </script>
    @endif

    <script>
        $('.formulario-eliminar').submit(function(e){
            e.preventDefault();
            Swal.fire({
                title: "¿Seguro?",
                text: "Esto no se puede deshacer",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Sí, borrar"
            }).then((result) => {
                if (result.isConfirmed) {
                    this.submit();
                }
            });
        })
    </script>
@endsection
