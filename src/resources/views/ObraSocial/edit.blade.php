@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Editar Obra Social</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('ObraSocial.update', $obraSocial->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input id="nombre" type="text" class="form-control" name="nombre" value="{{ $obraSocial->nombre }}" required autofocus>
                            </div>

                            <!-- Otros campos del formulario -->

                            <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                            <a href="{{ route('ObraSocial.index') }}" class="btn btn-secondary">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
