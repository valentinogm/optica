@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Detalles del Pedido</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#clienteInfo">
                                    <span class="info-box-icon bg-info"><i class="fas fa-user"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Cliente</span>
                                        <span class="info-box-number">{{ $pedido->cliente->apellido }} {{ $pedido->cliente->nombre }}</span>
                                    </div>
                                </div>
                                <div id="clienteInfo" class="collapse">
                                    <p>DNI: {{ $pedido->cliente->dni }}</p>
                                    <p>Domicilio: {{ $pedido->cliente->domicilio }}</p>
                                    <p>Localidad: {{ $pedido->cliente->localidad }}</p>
                                    <p>Fecha de Nacimiento: {{ \Carbon\Carbon::parse($pedido->cliente->fecha_nac)->format('d-m-Y') }}</p>
                                    <p>Teléfono: {{ $pedido->cliente->telefono }}</p>
                                    <p>E-Mail: {{ $pedido->cliente->mail }}</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#codigoInfo">
                                    <span class="info-box-icon bg-secondary"><i class="fas fa-barcode"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Código</span>
                                        <span class="info-box-number">{{ $pedido->codigo }}</span>
                                    </div>
                                </div>
                                <div id="codigoInfo" class="collapse show">
                                    <!-- Agrega más detalles del código aquí -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#lentesInfo">
                                    <span class="info-box-icon bg-primary"><i class="fas fa-glasses"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Lentes</span>
                                        <div id="lentesInfo" class="collapse">
                                            <p><strong>Cerca:</strong></p>
                                            @if($pedido->cercalente)
                                            <p>Tipo de Lente: {{ $pedido->cercalente->tipoLente->nombre }}</p>
                                            <p>Tipo armazón: {{ $pedido->cercalente->tipo_armazon->nombre }}</p>
                                            <p>Cristal: {{ $pedido->cercalente->cristal->nombre }}</p>
                                            @else
                                            <p>No especificado</p>
                                            @endif

                                            <p><strong>Lejos:</strong></p>
                                            @if($pedido->lejoslente)
                                            <p>Tipo de Lente: {{ $pedido->lejoslente->tipoLente->nombre }}</p>
                                            <p>Tipo armazón: {{ $pedido->lejoslente->tipo_armazon->nombre }}</p>
                                            <p>Cristal: {{ $pedido->lejoslente->cristal->nombre }}</p>
                                            @else
                                            <p>No especificado</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#oftalmologoInfo">
                                    <span class="info-box-icon bg-success"><i class="fas fa-eye"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Oftalmólogo</span>
                                        <span class="info-box-number">{{ $pedido->oftalmologo->nombre }} {{ $pedido->oftalmologo->apellido }} - {{ $pedido->oftalmologo->matricula }} </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#laboratorioInfo">
                                    <span class="info-box-icon bg-warning"><i class="fas fa-flask"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Laboratorio</span>
                                        <span class="info-box-number">{{ $pedido->laboratorio->nombre }} - {{ $pedido->laboratorio->nro }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#vendedorInfo">
                                    <span class="info-box-icon bg-dark"><i class="fas fa-user-tie"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Vendedor</span>
                                        <span class="info-box-number">{{ $pedido->vendedor->name }} {{ $pedido->vendedor->apellido }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#obraSocialInfo">
                                    <span class="info-box-icon bg-warning"><i class="fas fa-hospital"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Obra Social</span>
                                        <span class="info-box-number">{{ $pedido->obraSocial->nombre }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#fechaPedidoInfo">
                                    <span class="info-box-icon bg-primary"><i class="fas fa-calendar-alt"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Fecha del Pedido</span>
                                        <span class="info-box-number">{{ \Carbon\Carbon::parse($pedido->fecha_pedido)->format('d-m-Y')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#fechaEstimadaInfo">
                                    <span class="info-box-icon bg-primary"><i class="fas fa-calendar-alt"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Fecha Estimada de Entrega<span>
                                        <span class="info-box-number">{{ \Carbon\Carbon::parse($pedido->fecha_estimada)->format('d-m-Y')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info-box" data-toggle="collapse" data-target="#saldoSeniaInfo">
                                    <span class="info-box-icon bg-danger"><i class="fas fa-money-bill-wave"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-number">
                                            Importe, Saldo y Seña
                                        </span>
                                    </div>
                                </div>
                                <div id="saldoSeniaInfo" class="collapse">
                                    <p>Importe: ${{ number_format($pedido->importe, 2, ',', '.') }}</p>
                                    <p>Seña: ${{ number_format($pedido->senia, 2, ',', '.') }}</p>
                                    <p>Saldo: ${{ number_format($pedido->saldo, 2, ',', '.') }}</p>                                    
                                </div>
                            </div>
                            @if($pedido->fecha_entrega)
                                <div class="col-md-4">
                                    <div class="info-box" data-toggle="collapse" data-target="#fechaEntregaInfo">
                                        <span class="info-box-icon bg-success"><i class="fas fa-calendar-check"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Fecha de Entrega</span>
                                            <span class="info-box-number">{{ \Carbon\Carbon::parse($pedido->fecha_entrega)->format('d-m-Y') }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <!-- Agrega más cuadros de información según sea necesario -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="info-box" data-toggle="collapse" data-target="#observacionesInfo">
                                        <span class="info-box-icon bg-info"><i class="fas fa-sticky-note"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Observaciones</span>
                                        </div>
                                    </div>
                                    <div id="observacionesInfo" class="collapse">
                                        <p>{{ $pedido->observaciones }}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Receta 1 -->
                            @if($pedido->receta)
                                <div class="col-md-4" data-toggle="modal" data-target="#imagenModal">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-secondary"><i class="fas fa-image" style="font-size: 30px;"></i></span>
                                        <div class="info-box-content">
                                            <h5 class="info-box-text"><strong>Receta</strong></h5>
                                            <div class="info-box-number">
                                                <p>Click para abrir y ver la receta</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <!-- Receta 2 -->
                        @if($pedido->receta_2)
                                <div class="col-md-4" data-toggle="modal" data-target="#imagenModal2">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-secondary"><i class="fas fa-image" style="font-size: 30px;"></i></span>
                                        <div class="info-box-content">
                                            <h5 class="info-box-text"><strong>Imagen adicional</strong></h5>
                                            <div class="info-box-number">
                                                <p>Click para abrir y ver la imagen adicional</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <!-- Receta 1  -->
                        <div class="modal fade" id="imagenModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <img src="{{ asset('storage/' . $pedido->receta) }}" alt="Receta" style="width: 100%;">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Receta 2 -->
                        <div class="modal fade" id="imagenModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <img src="{{ asset('storage/' . $pedido->receta_2) }}" alt="Receta" style="width: 100%;">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <a href="{{ route('pedido.index') }}" class="btn btn-primary">Volver</a>
                            <a href="{{ route('pedido.created', $pedido->id) }}" class="btn btn-secondary ml-2">Ver Recibo</a>
                            <a href="{{ route('pedido.lab', $pedido->id) }}" class="btn btn-secondary ml-2">Recibo Laboratorio</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
