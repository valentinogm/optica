@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Crear Nuevo Pedido</div>

                    <div class="card-body">
                        <!-- Mostrar errores de validación -->
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form method="POST" action="{{ route('pedido.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <!-- Cliente  -->
                                    <label for="cliente">Cliente</label>
                                    <select class="form-control" id="cliente" name="cliente" required></select>

                                    <!-- Botón para agregar nuevo cliente -->
                                    <button type="button" class="btn btn-primary mt-2" data-toggle="modal" data-target="#modalAgregarCliente">
                                        Agregar Cliente
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <!-- Oftalmólogo -->
                                    <label for="oftalmologo">Oftalmólogo</label>
                                    <select id="oftalmologo" class="form-control" name="oftalmologo" required>
                                        <option value="">Selecciona un oftalmólogo</option>
                                        @foreach($oftalmologos as $oftalmologo)
                                            <option value="{{ $oftalmologo->id }}">{{ $oftalmologo->nombre }} {{ $oftalmologo->apellido }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <!-- Laboratorio -->
                                    <label for="laboratorio">Laboratorio</label>
                                    <select id="laboratorio" class="form-control" name="laboratorio" required>
                                        <option value="">Selecciona un laboratorio</option>
                                        @foreach($laboratorios as $laboratorio)
                                            <option value="{{ $laboratorio->id }}">{{ $laboratorio->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <!-- Obra Social -->
                                    <label for="obra_social">Obra Social</label>
                                    <select id="obra_social" class="form-control" name="obra_social" required>
                                        <option value="">Selecciona una obra social</option>
                                        @foreach($obras_sociales as $obra_social)
                                            <option value="{{ $obra_social->id }}">{{ $obra_social->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- Importe -->
                            <div class="form-group">
                                <label for="importe">Importe</label>
                                <input id="importe" type="number" class="form-control" name="importe" required>
                            </div>

                            <!-- Seña -->
                            <div class="form-group">
                                <label for="senia">Seña</label>
                                <input id="senia" type="number" class="form-control" name="senia" required>
                            </div>
                            <!-- Observaciones -->
                            <div class="form-group">
                                <label for="observaciones">Observaciones:</label>
                                <textarea class="form-control" id="observaciones" name="observaciones" rows="3"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Lente de Cerca -->
                                    <div class="form-group" id="lente_cerca">
                                        <h4>Opción: Lente de Cerca</h4>
                                        <div class="form-group">
                                            <label for="tipo_lente_cerca">Tipo de Lente:</label>
                                            <select id="tipo_lente_cerca" class="form-control" name="lente_cerca[tipo_lente]">
                                                <option value="">Seleccione un tipo de lente</option>
                                                @foreach($tipos_lente as $tipo_lente)
                                                    <option value="{{ $tipo_lente->id }}">{{ $tipo_lente->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="tipo_armazon_cerca">Tipo de Armazón:</label>
                                            <select id="tipo_armazon_cerca" class="form-control" name="lente_cerca[tipo_armazon]">
                                                <option value="">Seleccione un tipo de armazón</option>
                                                @foreach($tipo_armazon as $armazon)
                                                    <option value="{{ $armazon->id }}">{{ $armazon->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="cristal_cerca">Cristal:</label>
                                            <select id="cristal_cerca" class="form-control" name="lente_cerca[cristal]">
                                                <option value="">Seleccione un tipo de cristal</option>
                                                @foreach($cristal as $crist)
                                                    <option value="{{ $crist->id }}">{{ $crist->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                    <!-- Lente de Lejos -->
                                    <div class="form-group" id="lente_lejos">
                                        <h4>Opción: Lente de Lejos</h4>
                                        <div class="form-group">
                                            <label for="tipo_lente_lejos">Tipo de Lente:</label>
                                            <select id="tipo_lente_lejos" class="form-control" name="lente_lejos[tipo_lente]">
                                                <option value="">Seleccione un tipo de lente</option>
                                                @foreach($tipos_lente as $tipo_lente)
                                                    <option value="{{ $tipo_lente->id }}">{{ $tipo_lente->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="tipo_armazon_lejos">Tipo de Armazón:</label>
                                            <select id="tipo_armazon_lejos" class="form-control" name="lente_lejos[tipo_armazon]">
                                                <option value="">Seleccione un tipo de armazón</option>
                                                @foreach($tipo_armazon as $armazon)
                                                    <option value="{{ $armazon->id }}">{{ $armazon->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="cristal_lejos">Cristal:</label>
                                            <select id="cristal_lejos" class="form-control" name="lente_lejos[cristal]">
                                                <option value="">Seleccione un tipo de cristal</option>
                                                @foreach($cristal as $crist)
                                                    <option value="{{ $crist->id }}">{{ $crist->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <!-- tipo -->
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="tipo" name="tipo" onchange="updatePlazo()">
                                    <label class="form-check-label" for="tipo">
                                        ¿El pedido incluye laboratorio?
                                    </label>
                                </div>
                            </div>
                            <p>Plazo de entrega: <span id="plazo">4 días</span></p>
                            <!-- Recete medica -->
                            <div class="form-group">
                                <label for="receta">Receta Médica</label>
                                <input type="file" class="form-control-file" id="receta" name="receta" onchange="previewImage(event)">
                                <!-- Vista previa de la imagen -->
                                <img id="preview" src="#" alt="Vista previa de la imagen" style="max-width: 100%; max-height: 200px; margin-top: 10px; display: none;">
                            </div>

                            <!-- Recete medica 2-->
                            <div class="form-group">
                                <label for="receta_2">Receta Médica (segunda imagen)</label>
                                <input type="file" class="form-control-file" id="receta_2" name="receta_2" onchange="previewImage2(event)">
                                <!-- Vista previa de la imagen -->
                                <img id="preview2" src="#" alt="Vista previa de la imagen" style="max-width: 100%; max-height: 400px; margin-top: 20px; display: none;">
                            </div>  

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary">Crear Pedido</button>
                                        <a href="{{ route('pedido.index') }}" class="btn btn-secondary">Cancelar</a>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal para agregar nuevo cliente -->
    <div class="modal fade" id="modalAgregarCliente" tabindex="-1" role="dialog" aria-labelledby="modalAgregarClienteLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAgregarClienteLabel">Agregar Nuevo Cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Formulario para agregar un nuevo cliente -->
                    <form action="{{ route('Clientes.store') }}" method="POST" id="formNuevoCliente">
                        @csrf
                        <div class="form-group">
                            <label for="dni">DNI:</label>
                            <input type="text" class="form-control" id="dni" name="dni" required>
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" required>
                        </div>
                        <div class="form-group">
                            <label for="apellido">Apellido:</label>
                            <input type="text" class="form-control" id="apellido" name="apellido" required>
                        </div>
                        <div class="form-group">
                            <label for="domicilio">Domicilio:</label>
                             <input type="text" class="form-control" id="domicilio" name="domicilio" required>
                          </div>
                            <div class="form-group">
                              <label for="localidad">Localidad:</label>
                                <input type="text" class="form-control" id="localidad" name="localidad" required>
                            </div>
                            <div class="form-group">
                                <label for="fecha_nac">Fecha de Nacimiento:</label>
                                <input type="date" class="form-control" id="fecha_nac" name="fecha_nac" required>
                            </div>
                            <div class="form-group">
                                <label for="telefono">Teléfono:</label>
                                <input type="text" class="form-control" id="telefono" name="telefono" >
                            </div>
                            <div class="form-group">
                                <label for="mail">Correo Electrónico:</label>
                                <input type="email" class="form-control" id="mail" name="mail">
                        <!-- Otros campos del formulario -->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="guardarCliente">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Script para enviar los datos del nuevo cliente al servidor -->
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Manejo del evento click en el botón "Guardar"
            document.getElementById('guardarCliente').addEventListener('click', function() {
                // Envía los datos del formulario al servidor usando AJAX
                $.ajax({
                    url: '{{ route("Clientes.store") }}', // URL del endpoint para guardar un nuevo cliente
                    method: 'POST',
                    data: $('#formNuevoCliente').serialize(), // Datos del formulario serializados
                    success: function(response) {
                        // Maneja la respuesta del servidor (puedes actualizar la lista de clientes, etc.)
                        console.log(response);
                        // Cierra el modal después de guardar el cliente
                        $('#modalAgregarCliente').modal('hide');
                    },
                    error: function(error) {
                        console.error('Error al guardar el cliente:', error);
                    }
                });
            });
        });
    </script>
@endsection

@section('css')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"/>
<style>
    /* Estilos para el campo de selección de clientes */
    #search {
        width: 100%; /* Ocupa todo el ancho del contenedor */
        height: 38px; /* Altura deseada */
        font-size: 14px; /* Tamaño de fuente deseado */
        color: #495057; /* Color de texto deseado */
        border: 1px solid #ced4da; /* Borde del campo */
        border-radius: .25rem; /* Borde redondeado */
        background-color: #fff; /* Color de fondo deseado */
        padding: .375rem .75rem; /* Espaciado interno */
        box-sizing: border-box; /* Incluye el padding en el ancho total */
    }

    /* Estilos para el campo de selección cuando está enfocado */
    #search:focus {
        border-color: #80bdff; /* Color de borde deseado cuando está enfocado */
    }

    /* Estilos adicionales para otros elementos */
    .select2-container--default .select2-selection--single {
        height: 38px; /* Altura deseada */
        font-size: 14px; /* Tamaño de fuente deseado */
    }

    /* Estilos adicionales para hacerlo responsive */
    @media (max-width: 768px) {
        #search {
            font-size: 12px; /* Tamaño de fuente ajustado para dispositivos móviles */
            height: 32px; /* Altura ajustada para dispositivos móviles */
        }
    }
</style>
@endsection


@section('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>  

    
    <script>  
       var path = "{{ route('pedido.clientesobtener') }}";

        $('#cliente').select2({
            placeholder: 'Seleccionar usuario',
            ajax: {
                url: path,
                dataType: 'json',
                delay: 250,
                processResults: function (clientes) {
                    return {
                        results:  $.map(clientes, function (item) {
                            return {
                                text: item.nombre + ' ' + item.apellido + ' - ' + item.dni,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    </script>

    <script>
        $('#oftalmologo').select2({
            placeholder: 'Buscar un oftalmólogo',
            data: [
                @foreach($oftalmologos as $oftalmologo)
                    { id: '{{ $oftalmologo->id }}', text: '{{ $oftalmologo->nombre }} {{ $oftalmologo->apellido }}' },
                @endforeach
            ]
        });

        $('#laboratorio').select2({
            placeholder: 'Buscar un laboratorio',
            data: [
                @foreach($laboratorios as $laboratorio)
                    { id: '{{ $laboratorio->id }}', text: '{{ $laboratorio->nombre }}' },
                @endforeach
            ]
        });

        $('#obra_social').select2({
            placeholder: 'Buscar Obra Social',
            data: [
                @foreach($obras_sociales as $obra_social)
                    { id: '{{ $obra_social->id }}', text: '{{ $obra_social->nombre }}' },
                @endforeach
            ]
        });
    </script>

    <script>
        function updatePlazo() {
            var checkbox = document.getElementById("tipo");
            var plazo = document.getElementById("plazo");

            if (checkbox.checked) {
                // Si el checkbox está marcado (incluye laboratorio), plazo es de 15 días
                plazo.textContent = "15 días";
            } else {
                // Si el checkbox no está marcado (no incluye laboratorio), plazo es de 4 días
                plazo.textContent = "4 días";
            }
        }
    </script>
        <!-- JavaScript para mostrar la vista previa de la imagen -->
    <script>
        function previewImage(event) {
            var input = event.target;
            var preview = document.getElementById('preview');

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    preview.src = e.target.result;
                    preview.style.display = 'block'; // Mostrar la vista previa
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function previewImage2(event) {
            var input = event.target;
            var preview = document.getElementById('preview2');

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    preview.src = e.target.result;
                    preview.style.display = 'block'; // Mostrar la vista previa
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            function updateRequiredFields(sectionId) {
                const section = document.getElementById(sectionId);
                const selects = section.querySelectorAll('select');
                let anySelected = false;

                selects.forEach(select => {
                    if (select.value) {
                        anySelected = true;
                    }
                });

                selects.forEach(select => {
                    if (anySelected) {
                        select.required = true;
                    } else {
                        select.required = false;
                    }
                });
            }

            const cercaSelects = document.querySelectorAll('#lente_cerca select');
            const lejosSelects = document.querySelectorAll('#lente_lejos select');

            cercaSelects.forEach(select => {
                select.addEventListener('change', function() {
                    updateRequiredFields('lente_cerca');
                });
            });

            lejosSelects.forEach(select => {
                select.addEventListener('change', function() {
                    updateRequiredFields('lente_lejos');
                });
            });
        });
    </script>
@endsection