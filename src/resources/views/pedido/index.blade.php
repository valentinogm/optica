@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="page-header">
                    <br>
                    <hr>
                    <div class="d-flex flex-wrap justify-content-starts">
                        <a class="btn btn-success mb-2" href="{{ route('pedido.create') }}">Nuevo Pedido&nbsp;&nbsp;<span
                                class="fa fa-plus"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Gestión de Pedidos</div>
                    <div class="card-body">
                        <form action="{{ route('pedido.index') }}" method="GET" class="form-inline mb-3">
                            <input class="form-control mr-sm-2 mb-2" type="search" placeholder="Buscar por ID"
                                aria-label="Buscar" name="q" value="{{ request('q') }}">
                            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Buscar</button>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Cliente</th>
                                        <th>Telefono</th>
                                        <th>DNI</th>
                                        <th>O. Social</th>
                                        <th>Fecha Pedido</th>
                                        <th>Fecha Estimada De Entrega</th>
                                        <th>Estado Actual</th>
                                        <th class="text-right">Herramientas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--  && $pedido->tipo != 'No Incluye laboratorio' -->
                                    @foreach ($pedidos as $pedido)
                                        <tr
                                            @if (
                                                \Carbon\Carbon::parse($pedido->fecha_estimada)->diffInDays(\Carbon\Carbon::now()) <= 10 &&
                                                    $pedido->estadoActual != 'TERMINADO' &&
                                                    $pedido->estadoActual != 'PASÓ CONTROL DE CALIDAD') style="background-color: rgba(255, 0, 0, 0.3);"
        @elseif($pedido->estadoActual === 'PASÓ CONTROL DE CALIDAD')
            style="background-color: rgba(255, 255, 0, 0.3);"
        @elseif(
            \Carbon\Carbon::parse($pedido->fecha_estimada)->diffInDays(\Carbon\Carbon::now()) <= 20 &&
                $pedido->estadoActual != 'TERMINADO' &&
                $pedido->estadoActual != 'PASÓ CONTROL DE CALIDAD')
            style="background-color: rgba(255, 165, 0, 0.3);"
        @elseif($pedido->estadoActual === 'TERMINADO')
            style="background-color: rgba(0, 255, 0, 0.3);" @endif>
                                            <td>{{ $pedido->codigo }}</td>
                                            <td>{{ $pedido->cliente->apellido }} {{ $pedido->cliente->nombre }}</td>
                                            <td>{{ $pedido->cliente->telefono }}</td>
                                            <td>{{ $pedido->cliente->dni }}</td>
                                            <td>{{ $pedido->obraSocial->nombre }}</td>
                                            <td>{{ \Carbon\Carbon::parse($pedido->fecha_pedido)->format('d-m-Y') }}</td>
                                            <td>{{ \Carbon\Carbon::parse($pedido->fecha_estimada)->format('d-m-Y') }}</td>
                                            <td>{{ $pedido->estadoActual }}</td>
                                            <td class="text-right">
                                                <!-- Botón de menú desplegable -->
                                                <div class="dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                        Acciones
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item"
                                                            href="{{ route('pedido.edit', $pedido->id) }}">Editar</a>
                                                        @can('pedido.destroy')
                                                            <form action="{{ route('pedido.destroy', $pedido->id) }}"
                                                                class="d-inline formulario-eliminar" method="POST">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit" class="dropdown-item text-danger"
                                                                    style="border: none; background: none;">
                                                                    Borrar
                                                                </button>
                                                            </form>
                                                        @endcan
                                                        <a class="dropdown-item"
                                                            href="{{ route('pedido.show', $pedido->id) }}">Ver</a>
                                                        <a class="dropdown-item"
                                                            href="{{ route('pedido.verestados', $pedido->id) }}">Ver
                                                            estados</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="float-right">
        {{ $pedidos->appends(request()->query())->links() }}
    </div>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    @if (session('success') == 'ok')
        <script>
            Swal.fire({
                position: "top-end",
                icon: "success",
                title: "Se guardó con éxito",
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif

    @if (session('eliminar') == 'ok')
        <script>
            Swal.fire({
                title: "Eliminado!",
                text: "Se eliminó correctamente.",
                icon: "success"
            });
        </script>
    @endif;

    <script>
        $('.formulario-eliminar').submit(function(e) {
            e.preventDefault();
            Swal.fire({
                title: "Seguro?",
                text: "Esto no se puede deshacer",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Si, borrar"
            }).then((result) => {
                if (result.isConfirmed) {
                    this.submit();
                }
            });
        })
    </script>
@endsection
