@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td style="font-size: 24px; text-align: left;">
                                <strong>Nombre:</strong> {{ $pedido->cliente->apellido }}, {{ $pedido->cliente->nombre }}
                            </td>
                            <td style="font-size: 24px; text-align: left;">
                                <strong>Documento:</strong> {{ $pedido->cliente->dni }}
                            </td>
                        </tr>
                    </table>
                    <table class="table">
                        <tr>
                            <th>Obra Social</th>
                            <td>{{ $pedido->obraSocial->nombre }}</td>
                            <th>Laboratorio</th>
                            <td>{{ $pedido->laboratorio->nombre }}</td>
                            <th>Fecha del Pedido:</th>
                            <td>{{ \Carbon\Carbon::parse($pedido->fecha_pedido)->format('d-m-Y') }}</td>
                            <th>Fecha Estimada de Entrega:</th>
                            <td>{{ \Carbon\Carbon::parse($pedido->fecha_estimada)->format('d-m-Y') }}</td>
                        </tr>
                        @if($pedido->lejoslente)
                        <tr class="highlighted-row">
                            <th>Tipo de Lente (Lejos):</th>
                            <td>{{ $pedido->lejoslente->tipoLente->nombre }}</td>
                            <th>Tipo de armazón (Lejos):</th>
                            <td>{{ $pedido->lejoslente->tipo_armazon->nombre }}</td>
                            <th>Cristal (Lejos):</th>
                            <td>{{ $pedido->lejoslente->cristal->nombre }}</td>
                        </tr>
                        @endif
                        @if($pedido->cercalente)
                        <tr class="highlighted-row">
                            <th>Tipo de Lente (Cerca):</th>
                            <td>{{ $pedido->cercalente->tipoLente->nombre }}</td>
                            <th>Tipo de armazón (Cerca):</th>
                            <td>{{ $pedido->cercalente->tipo_armazon->nombre }}</td>
                            <th>Cristal (Cerca):</th>
                            <td>{{ $pedido->cercalente->cristal->nombre }}</td>
                        </tr>
                        @endif
                        @if($pedido->observaciones)
                            <tr>
                                <th>Observaciones</th>
                                <td>{{ $pedido->observaciones }}</td>
                            </tr>
                        @endif
                    </table>
                    @if($pedido->receta)
                    <div class="image-container">
                        <img src="{{ asset('storage/' . $pedido->receta) }}" alt="Receta del Pedido" class="receta-img">
                    </div>
                    @endif
                </div>
                <div class="card-footer" id="botones-impresion">
                    <div class="row">
                        <div class="col">
                        <a href="{{ url()->previous() == route('pedido.show', ['id' => $pedido->id]) ? url()->previous() : route('pedido.index') }}" class="btn btn-primary">Volver</a>
                        </div>
                        <div class="col text-right">
                            <a href="#" class="btn btn-primary" onclick="window.print()"><i class="fas fa-print"></i> Imprimir</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
    <style>
        .image-container {
            text-align: center;
            width: 100%;
        }
        .receta-img {
            width: 100%;
            height: auto;
            max-height: 80vh; /* Limita la altura de la imagen para que no ocupe más del 80% del alto de la ventana */
            object-fit: contain; /* Escala la imagen sin recortar */
        }
        /* Estilos para ocultar los botones de impresión al imprimir */
        @media print {
            #botones-impresion {
                display: none;
            }
            .receta-img {
                width: 100%;
                height: auto;
                max-height: 90vh; /* Limita la altura de la imagen para que no ocupe más del 90% del alto de la página */
            }
        }
        .highlighted-row {
        border: 2px solid #000; /* Color y grosor del borde */
        background-color: #f0f0f0; /* Color de fondo opcional */
        }

        .highlighted-row th,
        .highlighted-row td {
            padding: 10px;
        }
    </style>
@endsection
