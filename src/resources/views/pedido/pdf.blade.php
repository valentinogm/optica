<!DOCTYPE html>
<html>
<head>
    <title>Reporte de Pedidos</title>
    <style>
        /* Estilos CSS para el PDF */
        body {
            font-family: Arial, sans-serif;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h1>Reporte de Pedidos</h1>
    <table>
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Cliente</th>
                <th>DNI</th>
                <th>Fecha Pedido</th>
                <th>Fecha Estimada De Entrega</th>
                <th>Estado Actual</th>
                <th>Oftalmologo</th>
                <th>Obra Social</th>
                <th>Importe total</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pedidos as $pedido)
                <tr>
                    <td>{{ $pedido->codigo }}</td>
                    <td>{{ $pedido->cliente->apellido }} {{ $pedido->cliente->nombre }}</td>
                    <td>{{ $pedido->cliente->dni }}</td>
                    <td>{{ \Carbon\Carbon::parse($pedido->fecha_pedido)->format('d-m-Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($pedido->fecha_estimada)->format('d-m-Y') }}</td>
                    <td>{{ $pedido->estadoActual }}</td>
                    <td>{{ $pedido->oftalmologo->nombre }} {{ $pedido->oftalmologo->apellido }} | {{ $pedido->oftalmologo->matricula }}</td>
                    <td>{{ $pedido->obraSocial->nombre }}</td>
                    <td>${{ number_format($pedido->importe, 2, ',', '.') }}</td>
                </tr>
            @endforeach
        </tbody>
    
