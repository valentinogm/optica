@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <!-- Información de contacto y logo -->
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-md-4">
                            <img src="{{ asset('storage/logos/logo.png') }}" alt="Ópticas Schellhas" style="max-height: 100px;">
                        </div>
                        <div class="col-md-4 text-center">
                            <h4 style="margin: 0;">RECIBO <span style="font-weight: bold;">X</span></h4>
                            <p style="margin: 0;">DOCUMENTO NO VÁLIDO COMO FACTURA</p>
                        </div>
                        <div class="col-md-4 text-right">
                            <p><i class="fab fa-whatsapp"></i> 343 471 5911 | ☎️ 343 432 5824</p>
                            <p>📍 Urquiza 1043</p>
                        </div>
                    </div>
                </div>

                <!-- Título de Confirmación de Pedido -->
                <div class="card-body">
                    <h3 class="card-title">Confirmación de Pedido</h3>
                    <br>
                    <hr>
                    <p>Detalles del pedido</p>
                    <table class="table" >
                        <tr>
                            <td style="font-size: 24px; text-align: left;">
                                <strong>Nombre:</strong> {{ $pedido->cliente->apellido }}, {{ $pedido->cliente->nombre }}
                            </td>
                            <td style="font-size: 24px; text-align: left;">
                                <strong>Documento:</strong> {{ $pedido->cliente->dni }}
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <p><strong>Fecha del Pedido:</strong> {{ \Carbon\Carbon::parse($pedido->fecha_pedido)->format('d-m-Y') }}</p>
                    @if ($pedido->tipo == 'Incluye Laboratorio')
                        <p><strong>Fecha Estimada de Entrega:</strong> 15 días hábiles de la fecha del pedido</p>
                    @else
                        <p><strong>Fecha Estimada de Entrega:</strong> 4 días hábiles de la fecha del pedido</p>
                    @endif
                    <hr>
                    <p><strong>Importe Total:</strong> ${{ number_format($pedido->importe, 2, ',', '.') }}</p>
                    <p><strong>Seña Abonada:</strong> ${{ number_format($pedido->senia, 2, ',', '.') }}</p>
                    <p><strong>Saldo Pendiente:</strong> ${{ number_format($pedido->saldo, 2, ',', '.') }}</p>
                    <hr>
                </div>
                <!-- Nota sobre las señas recibidas -->
                <div class="card-body">
                    <p class="text-muted nota-final">
                        Las señas recibidas por cualquier forma de pago no serán devueltas. La entrega de los trabajos están sujetas a disponibilidad de stock de materiales. Los trabajos podrán ser cancelados dentro de las 24 hs de tomado el pedido.
                    </p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col">
                            <a href="{{ url()->previous() == route('pedido.show', ['id' => $pedido->id]) ? url()->previous() : route('pedido.index') }}" class="btn btn-primary">Volver</a>
                        </div>
                        <div class="col text-right">
                            <a href="#" class="btn btn-primary" onclick="printAndDuplicate()"><i class="fas fa-print"></i> Imprimir</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
    <style>
        /* Estilos CSS personalizados */
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }
        .container {
            margin-top: 20px;
        }

        .nota-final {
            font-size: 0.9em;
            margin-top: -50px;
        }

        /* Ocultar los botones al imprimir */
        @media print {
            .card-footer {
                display: none;
            }
            .card-header .row {
                display: flex;
                align-items: center;
            }
            .card-header .col-md-4 {
                flex: 1;
                text-align: left;
            }
            .card-header img {
                max-height: 100px;
                margin: 0;
            }
            .card-header .text-right {
                text-align: right;
                margin-left: auto;
            }
        }
    </style>
@endsection

@section('js')
    <script>
        function printAndDuplicate() {
            // Clonar el contenido de la tarjeta
            var cardClone = document.querySelector('.card').cloneNode(true);
            
            // Insertar el contenido clonado al final del cuerpo del documento
            document.body.appendChild(cardClone);
            
            // Establecer estilos para la copia
            cardClone.setAttribute('style', 'font-family: Arial, sans-serif; line-height: 1.6; margin: 0; padding: 0; background-color: #f4f4f4; margin-top: 20px;');
            
            // Imprimir el documento
            window.print();
            
            // Remover el contenido clonado después de la impresión
            cardClone.remove();
        }
    </script>
@endsection
