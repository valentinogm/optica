<!-- resources/views/reports/pdf.blade.php -->

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ordenes</title>
    <style>
        .orders-container {
            display: flex;
            flex-wrap: wrap;
            gap: 16px;
        }

        .order-card {
            border: 1px solid #ddd;
            border-radius: 8px;
            padding: 16px;
            background-color: #fff;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            width: calc(33.333% - 32px); /* Ajusta el ancho según tus necesidades */
            box-sizing: border-box;
        }

        .order-item {
            margin-bottom: 8px;
        }

        .order-item .label {
            font-weight: bold;
            color: #1b1b1b;
        }

        .order-item .value {
            color: #333;
        }

        @media (max-width: 768px) {
            .order-card {
                width: calc(50% - 16px);
            }
        }

        @media (max-width: 480px) {
            .order-card {
                width: 100%;
            }
        }

        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
    <div class="orders-container">
        @foreach($ordenes as $orden)
            <div class="order-card">
                <div class="order-item">
                    <span class="label">Cliente:</span>
                    <span class="value">{{ $orden->cliente->razon_social }}</span>
                </div>
                <div class="order-item">
                    <span class="label">Fabrica:</span>
                    <span class="value">{{ $orden->fabrica->nombre }}</span>
                </div>
                <div class="order-item">
                    <span class="label">Estado:</span>
                    <span class="value">{{ $orden->estado->nombre }}</span>
                </div>
                <div class="order-item">
                    <span class="label">Fecha Asignacion:</span>
                    <span class="value">{{ $orden->fecha_asignacion }}</span>
                </div>
                <div class="order-item">
                    <span class="label">Fecha Finalizacion:</span>
                    <span class="value">{{ $orden->fecha_finalizacion }}</span>
                </div>
                <div class="order-item">
                    <span class="label">Observaciones:</span>
                    <span class="value">{{ $orden->observaciones }}</span>
                </div>
                <!-- Agrega aquí más items según tus necesidades -->
            </div>
            @if(($loop->iteration % 4) == 0) <!-- Por ejemplo, cada 3 ordenes -->
                <div class="page-break"></div>
            @endif
        @endforeach
    </div>
</body>
</html>
