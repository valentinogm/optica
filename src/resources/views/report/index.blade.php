@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Reportes</div>
                    <div class="card-body">
                        @csrf
                        
                        <!-- Reporte 1 -->
                        <div class="card mb-4">
                            <div class="card-header">
                                Reporte general - por periodo solicitado.
                            </div>
                            <div class="card-body">
                                {!! Form::open([
                                    'route' => 'reports.generate',
                                    'method' => 'POST',
                                    'style' => 'margin-left:3%',
                                    'class' => 'row gy-2 gx-3 align-items-center',
                                    'role' => 'search',
                                ]) !!}
                                <div class="col-auto form-group">
                                    {{ Form::label('date_from', 'Desde: (*)') }}
                                    {{ Form::date('date_from', null, ['class' => 'form-control', 'required' => 'required']) }}
                                </div>
                                <div class="col-auto form-group">
                                    {{ Form::label('date_to', 'Hasta: (*)') }}
                                    {{ Form::date('date_to', null, ['class' => 'form-control', 'required' => 'required']) }}
                                </div>
                                <div id="buscar" style="margin-top:2%" class="col-auto form-group">
                                    <button type="submit" class="btn btn-success">Generar Reporte
                                        <i class="fa fa-file-excel"></i>
                                    </button>
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#infoModal">
                                        Información del Reporte
                                    </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    <!-- Modales -->
                    <!-- Modal 1 -->
                    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="infoModalLabel">Información del Reporte</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Este reporte permite obtener en formato Excels , todos los pedidos con
                                        fecha de inicio en los valores establecidos en los campos editables, Desde y Hasta.
                                        por ejemplo si quieramos conocer las ordenes que iniciaron en el periodo 01/01/2023
                                        al 31/01/2023
                                    </p>
                                    <p>Si se quiere ver esta información pero del mismo dia, los valores se cargan asi
                                        01/01/2023 al 01/01/2023</p>
                                    <p>La información que se va a obtener con este reporte es información general de todos 
                                        los pedidos cargados en el periodo seleccionado</p>
                                    <ul>
                                        <li><strong>Desde:</strong> Fecha de inicio del reporte. (campo obligatorio)</li>
                                        <li><strong>Hasta:</strong> Fecha de fin del reporte. (campo obligatorio)</li>
                                        <!-- Agrega más detalles según sea necesario -->
                                    </ul>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            background-color: #f2f2f2;
            /* Cambia el color de fondo de la tabla a celeste */
        }

        th,
        td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: SteelBlue;
        }
    </style>
@endsection
