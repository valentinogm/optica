<!DOCTYPE html>
<html>
<head>
    <title>Reporte de Órdenes de Producción</title>
</head>
<style>
    .body {
        font-family: Arial, sans-serif;
    }
    .container {
        width: 100%;
        margin: 0 auto;
        padding: 20px;
    }
    .header {
        text-align: center;
        margin-bottom: 20px;
    }
    .table {
        width: 100%;
        border-collapse: collapse;
        margin-bottom: 20px;
    }
    .table th, .table td {
        border: 1px solid #ddd;
        padding: 8px;
    }
    .table th {
        background-color: #f2f2f2;
        text-align: left;
    }
</style>
<body>
    <div class="container">
        <p>Informe de totales de cantidades de elementos cargados en el sistema.
            Se detalla totales de ordenes de produccion y de algunos estados, totales de clientes,
            totales de proveedores.
            Total de productos, discriminando por materia prima y productos compuestos.
            
        </p>
        <div class="header">
            <h2>Órdenes de Producción</h2>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Estado</th>
                    <th>Cantidad</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Preparacion</td>
                    <td>{{ $preparacion_count }}</td>
                </tr>
                <tr>
                    <td>Finalizado</td>
                    <td>{{ $finalizado_count }}</td>
                </tr>
                <tr>
                    <td>Entregado</td>
                    <td>{{ $entregado_count }}</td>
                </tr>
                <tr>
                    <td>Total, todas las ordenes</td>
                    <td>{{ $ordenes_cantidad }}</td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="header">
            <h2>Clientes y proveedores (Cantidades)</h2>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Clientes y proveedores </th>
                    <th>Totales</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Total clientes</td>
                    <td>{{ $clientes_cantidad }}</td>
                </tr>
                <tr>
                    <td>Total proveedores</td>
                    <td>{{ $proveedores_cantidad }}</td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="header">
            <h2>Productos(Cantidades)</h2>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Materias primas y compuestos</th>
                    <th>Totales</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Total materias prima</td>
                    <td>{{ $materia_prima }}</td>
                </tr>
                <tr>
                    <td>Total compuestos</td>
                    <td>{{ $compuesto }}</td>
                </tr>
                <tr>
                    <td>Total de productos</td>
                    <td>{{ $productos_cantidad }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>