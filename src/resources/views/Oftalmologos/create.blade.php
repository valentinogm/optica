@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Crear Nuevo Oftalmólogo</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('Oftalmologo.store') }}">
                            @csrf

                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input id="nombre" type="text" class="form-control" name="nombre" required autofocus>
                            </div>

                            <div class="form-group">
                                <label for="apellido">Apellido</label>
                                <input id="apellido" type="text" class="form-control" name="apellido" required>
                            </div>

                            <div class="form-group">
                                <label for="matricula">Matrícula</label>
                                <input id="matricula" type="text" class="form-control" name="matricula" required>
                            </div>

                            <button type="submit" class="btn btn-primary">Crear Oftalmólogo</button>
                            <a href="{{ route('Oftalmologo.index') }}" class="btn btn-secondary">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
