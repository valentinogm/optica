@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Editar Laboratorio</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('Laboratorio.update', $laboratorio->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input id="nombre" type="text" class="form-control" name="nombre" value="{{ $laboratorio->nombre }}" required autofocus>
                            </div>

                            <div class="form-group">
                                <label for="nro">Número</label>
                                <input id="nro" type="text" class="form-control" name="nro" value="{{ $laboratorio->nro }}" required>
                            </div>

                            <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                            <a href="{{ route('Laboratorio.index') }}" class="btn btn-secondary">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
