@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Crear Nuevo Laboratorio</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('Laboratorio.index') }}">
                            @csrf

                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input id="nombre" type="text" class="form-control" name="nombre" required autofocus>
                            </div>

                            <div class="form-group">
                                <label for="nro">Numero</label>
                                <input id="nro" type="text" class="form-control" name="nro" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Crear Laboratorio</button>
                            <a href="{{ route('Oftalmologo.index') }}" class="btn btn-secondary">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
