@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Crear Nuevo Tipo de Armazon</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('tipo_armazon.store') }}">
                            @csrf
                            
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label for="tipo_material">Tipo de Material</label>
                                <input id="tipo_material" type="text" class="form-control @error('tipo_material') is-invalid @enderror" name="tipo_material" value="{{ old('tipo_material') }}" required autofocus>

                                @error('tipo_material')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            

                            <button type="submit" class="btn btn-primary">Crear Tipo de Armazon</button>
                            <a href="{{ route('tipo_armazon.index') }}" class="btn btn-secondary">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
