@extends('adminlte::page')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="page-header">
                    <br>
                    <hr>
                    <div class="d-flex flex-wrap justify-content-end">
                        <a class="btn btn-success mb-2" href="{{ route('tipo_armazon.create') }}">Nuevo&nbsp;&nbsp;<span class="fa fa-plus"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Gestión de Tipos de Armazón</div>
                    <div class="card-body">
                        <form action="{{ route('tipo_armazon.index') }}" method="GET" class="form-inline mb-3">
                            <input class="form-control mr-sm-2 mb-2" type="search" placeholder="Buscar por tipo" aria-label="Buscar" name="q" value="{{ request('q') }}">
                            <button class="btn btn-outline-primary my-2 my-sm-0 mb-2" type="submit">Buscar</button>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Tipo de material</th>
                                        <th class="text-right">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($tipoArmazones as $tipoArmazon)
                                        <tr>
                                            <td>{{ $tipoArmazon->id }}</td>
                                            <td>{{ $tipoArmazon->nombre }}</td>
                                            <td>{{ $tipoArmazon->tipo_material }}</td>
                                            <td class="text-right">
                                                <a href="{{ route('tipo_armazon.edit', $tipoArmazon->id) }}" class="btn btn-primary">Editar</a>
                                                @can('tipo_armazon.destroy')
                                                    <form action="{{ route('tipo_armazon.destroy', $tipoArmazon->id) }}" class="d-inline formulario-eliminar" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Borrar</button>
                                                    </form>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="float-right">
    {{ $tipoArmazones->links() }}
    </div>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    @if (session('success') == 'ok')
    <script>
        Swal.fire({
            position: "top-end",
            icon: "success",
            title: "Se guardó con éxito",
            showConfirmButton: false,
            timer: 1500
        });
    </script>
    @endif

    @if (session('eliminar')  == 'ok')
    <script>
        Swal.fire({
            title: "Eliminado!",
            text: "Se eliminó correctamente.",
            icon: "success"
        });
    </script>
    @endif

    <script>
        $('.formulario-eliminar').submit(function(e){
            e.preventDefault();
            Swal.fire({
                title: "¿Seguro?",
                text: "Esto no se puede deshacer",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Sí, borrar"
            }).then((result) => {
                if (result.isConfirmed) {
                    this.submit();
                }
            });
        })
    </script>
@endsection
