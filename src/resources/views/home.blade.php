@extends('adminlte::page')


@section('content_header')
    <h1>SGIP - SCHELLAS</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">¡Bienvenido a nuestro sistema de pedidos! SCHELLAS</div>
                <div class="card-body">
                    <p class="text-justify">Es de vital importancia mantener un control de la informacion manejada en este sistema.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    
@stop