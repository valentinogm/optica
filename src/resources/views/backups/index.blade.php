@extends('adminlte::page')

@section('content')
<br>
<hr>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">Adminitracion de backups</div>
                    <div class="card-body">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <div class="d-flex align-items-center">
                    <form action="{{ route('backups.create') }}" method="POST" class="mr-3">
                        @csrf
                        <button type="submit" class="btn btn-primary">Crear Backup</button>
                    </form>
                    <a href="{{ route('backups.clean') }}" class="btn btn-success mr-3">Limpiar Backups</a>
            </div>
            <table class="table table-striped mt-4">
                <thead>
                    <tr>
                        <th>Nombre del Archivo</th>
                        <th>Tamaño del Archivo</th>
                        <th>Última Modificación</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($backups as $backup)
                        <tr>
                            <td>{{ $backup['file_name'] }}</td>
                            <!-- <td>{{ $backup['file_size'] }} bytes</td> -->
                            <td>{{ $backup['file_size'] }} bytes ({{ number_format($backup['file_size'] / 1048576, 2) }} MB)</td>
                            <td>{{ $backup['last_modified'] }}</td>
                            <td>
                                <a href="{{ route('backups.download', $backup['file_name']) }}" class="btn btn-success">Descargar</a>
                                <form action="{{ route('backups.delete', $backup['file_name']) }}" method="POST" style="display:inline-block;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
