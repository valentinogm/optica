<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pedido Listo Para Retirar!</title>
    <style>
        /* Estilos CSS personalizados */
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }
        .container {
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        h2 {
            color: #333;
            border-bottom: 1px solid #ccc;
            padding-bottom: 10px;
            margin-bottom: 20px;
        }
        p {
            color: #666;
            margin-bottom: 10px;
        }
        strong {
            color: #000;
        }
        .footer {
            margin-top: 20px;
            padding-top: 20px;
            border-top: 1px solid #ccc;
            color: #888;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>¡Hola {{ $pedido->cliente->nombre }}!</h2>
        <p>Te informamos que tu pedido ya está listo para retirar. A continuación, te proporcionamos los detalles:</p>

        <p><strong>Código del Pedido:</strong> {{ $pedido->codigo }}</p>
        <p><strong>Fecha:</strong> {{ \Carbon\Carbon::parse($pedido->fecha_entrega)->format('d-m-Y') }}</p>
        <p>Gracias por confiar en nosotros. Lo esperamos.</p>

        <div class="footer">
            <p>Atentamente,<br>Ópticas Schellhas</p>
        </div>
    </div>
</body>
</html>
