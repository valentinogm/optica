<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo Pedido</title>
    <style>
        /* Estilos CSS personalizados */
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }
        .container {
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        h2 {
            color: #333;
        }
        p {
            color: #666;
        }
        .footer {
            margin-top: 20px;
            padding-top: 20px;
            border-top: 1px solid #ccc;
            color: #888;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>¡Hola {{ $pedido->cliente->nombre }}!</h2>
        <p>Te informamos que hemos recibido tu nuevo pedido. A continuación, te proporcionamos los detalles:</p>

        <p><strong>Código del Pedido:</strong> {{ $pedido->codigo }}</p>
        <p><strong>Fecha del Pedido:</strong> {{ \Carbon\Carbon::parse($pedido->fecha_pedido)->format('d-m-Y') }}</p>
        @if ($pedido->tipo == 'Incluye Laboratorio')
            <p><strong>Fecha Estimada de Entrega:</strong> 15 días hábiles de la fecha del pedido</p>
        @else
            <p><strong>Fecha Estimada de Entrega:</strong> 4 días hábiles de la fecha del pedido</p>
        @endif
        <p><strong>Importe Total:</strong> ${{ number_format($pedido->importe, 2, ',', '.') }}</p>
        <p><strong>Seña Abonada:</strong> ${{ number_format($pedido->senia, 2, ',', '.') }}</p>
        <p><strong>Saldo Pendiente:</strong> ${{ number_format($pedido->saldo, 2, ',', '.') }}</p>

        <p>Estamos trabajando para procesar tu pedido y te mantendremos informado sobre su estado.</p>

        <div class="footer">
            <p>Gracias por confiar en nosotros.</p>
            <p>Atentamente,<br>Ópticas Schellhas</p>
        </div>
    </div>
</body>
</html>
