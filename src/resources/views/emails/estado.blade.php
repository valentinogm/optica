<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Estado del Pedido Actualizado</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }

        .container {
            max-width: 800px;
            margin: 20px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        h1 {
            color: #333;
        }

        p {
            color: #666;
            margin-bottom: 15px;
        }

        ul {
            list-style-type: none;
            padding-left: 0;
        }

        li {
            margin-bottom: 5px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Estado del Pedido Actualizado</h1>
        <p>Estimado/a {{ $pedido->cliente->nombre }},</p>
        
        <p>Le informamos que el estado de su pedido ha sido actualizado.</p>
        
        <p>Detalles del pedido:</p>
        <ul>
            <li><strong>Código de Pedido:</strong> {{ $pedido->codigo }}</li>
            <li><strong>Estado Actual:</strong> {{ $estadodesc}}</li>
            <li><strong>Fecha de Actualización:</strong> {{ $estado->created_at->format('d-m-Y H:i:s') }}</li>
        </ul>
        
        <p>¡Gracias por confiar en nosotros!</p>
    </div>
</body>
</html>
