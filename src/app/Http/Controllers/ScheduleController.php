<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Filesystem\Filesystem;

class ScheduleController extends Controller
{
    public function showForm()
    {
        $kernelPath = app_path('Console/Kernel.php');
        $filesystem = new Filesystem();
        $cleanTime = '';
        $runTime = '';

        if ($filesystem->exists($kernelPath)) {
            $content = $filesystem->get($kernelPath);

            preg_match("/'backup:clean'\)->daily\(\)->at\('(\d{2}:\d{2})'\)/", $content, $cleanMatches);
            preg_match("/'backup:run'\)->daily\(\)->at\('(\d{2}:\d{2})'\)/", $content, $runMatches);

            $cleanTime = $cleanMatches[1] ?? '';
            $runTime = $runMatches[1] ?? '';
        }

        return view('schedule.form', compact('cleanTime', 'runTime'));
    }

    public function updateSchedule(Request $request)
    {
        $request->validate([
            'clean_time' => 'required|date_format:H:i',
            'run_time' => 'required|date_format:H:i',
        ]);

        $cleanTime = $request->input('clean_time');
        $runTime = $request->input('run_time');

        $kernelPath = app_path('Console/Kernel.php');
        $filesystem = new Filesystem();

        if ($filesystem->exists($kernelPath)) {
            $content = $filesystem->get($kernelPath);

            $scheduleFunction = "
    protected function schedule(Schedule \$schedule)
    {
        \$schedule->command('backup:clean')->daily()->at('$cleanTime');
        \$schedule->command('backup:run')->daily()->at('$runTime');
    }";

            $pattern = '/protected function schedule\(Schedule \$schedule\)\n    \{\n(.*?)\n    \}/s';

            if (preg_match($pattern, $content)) {
                $content = preg_replace($pattern, $scheduleFunction, $content);
            } else {
                $content = preg_replace('/(class Kernel extends ConsoleKernel\n\{)/', "$1\n$scheduleFunction", $content);
            }

            $filesystem->put($kernelPath, $content);

            return redirect()->back()->with('success', 'Las tareas programadas se han configurado correctamente.');
        } else {
            return redirect()->back()->with('error', 'No se pudo encontrar el archivo Kernel.php.');
        }
    }
}