<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\TipoArmazon;

class TipoArmazonController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:tipo_armazon.index')->only('index');
        $this->middleware('can:tipo_armazon.create')->only('create','store');
        $this->middleware('can:tipo_armazon.edit')->only('edit','update');
        $this->middleware('can:tipo_armazon.destroy')->only('destroy');
    }

    public function index(Request $request)
    {
        $query = $request->input('q');
        $tipoArmazones = tipoArmazon::where('nombre', 'ilike', '%' . $query . '%')
                                    ->paginate(10);
        return view('tipo_armazon.index', ['tipoArmazones' => $tipoArmazones]);
    }

    public function destroy(tipoArmazon $id)
    {
        $tipoArmazon = tipoArmazon::findOrFail($id->id);
        $user_id = Auth::id();
        $tipoArmazon->deleted_by_user_id = $user_id;
        $tipoArmazon->save();
        $tipoArmazon->delete();
        return redirect()->route('tipo_armazon.index')->with('eliminar', 'ok');
    }

    public function create()
    {
        return view('tipo_armazon.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255|unique:tipo_armazon',
            'tipo_material' => 'required|string|max:255|unique:tipo_armazon',
        ]);
        
        $tipoArmazon = new tipoArmazon();
        $tipoArmazon->nombre= $request->nombre;
        $tipoArmazon->tipo_material = $request->tipo_material;
        $tipoArmazon->save();
        return redirect()->route('tipo_armazon.index')->with('success', 'ok');

    }

    public function edit($id)
    {
        $tipoArmazon = tipoArmazon::findOrFail($id);
        return view('tipo_armazon.edit', compact('tipoArmazon'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string|max:255|unique:tipo_armazon,nombre,' . $id,
            'tipo_material' => 'required|string|max:255|unique:tipo_armazon,tipo_material,' . $id,
        ]);

        $tipoArmazon = tipoArmazon::findOrFail($id);
        $tipoArmazon->nombre= $request->nombre;
        $tipoArmazon->tipo_material = $request->tipo_material;
        $tipoArmazon->save();

        return redirect()->route('tipo_armazon.index')->with('success', 'ok');
    }
}
