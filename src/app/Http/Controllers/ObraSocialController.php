<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\ObraSocial;
use Illuminate\Http\JsonResponse;

class ObraSocialController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:ObraSocial.index')->only('index');
        $this->middleware('can:ObraSocial.create')->only('create','store');
        $this->middleware('can:ObraSocial.edit')->only('edit','update');
        $this->middleware('can:ObraSocial.destroy')->only('destroy');
    }
    public function index(Request $request)
    {
        $query = $request->input('q');
        $obrasSociales = ObraSocial::where('nombre', 'ilike', '%' . $query . '%')
                                    ->orderBy('nombre','asc')
                                    ->paginate(10);
        return view('ObraSocial.Index', ['obrasSociales' => $obrasSociales]);
    }

   public function destroy(ObraSocial $id){
        $ObraSocial = ObraSocial::findOrfail($id->id);
        $user_id = Auth::id();
        $ObraSocial->deleted_by_user_id = $user_id;
        $ObraSocial->save();
        $ObraSocial->delete();
        return redirect()->route('ObraSocial.index')->with('eliminar','ok');
   }

   public function create(){
    return view('ObraSocial.create');
   }

   public function store(request $request){
        $request->validate([
            'nombre' => 'required|string|max:255|unique:obra_social', // El campo nombre es único en la tabla obras_sociales
            // Agregar más reglas de validación según sea necesario para otros campos
        ]);
        $ObraSocial=new ObraSocial();
        $ObraSocial->nombre = $request->nombre;
        $ObraSocial->save();
        return redirect()->route('ObraSocial.index')->with('success', 'ok');
   }

   public function edit($id){
    $obraSocial = ObraSocial::findOrFail($id);
    return view('ObraSocial.edit', compact('obraSocial'));
    }

   public function update(Request $request, $id){
    $request->validate([
        'nombre' => 'required|string|max:255|unique:obra_social,nombre,' . $id,
    ]);

    $obraSocial = ObraSocial::findOrFail($id);
    $obraSocial->nombre = $request->nombre;
    // Actualizar otros campos según sea necesario
    $obraSocial->save();

    return redirect()->route('ObraSocial.index')->with('success', 'ok');
    }   

    public function obtenerObras(Request $request) : JsonResponse
    {
        $query = $request->input('q');
        $obras = ObraSocial::where('nombre', 'ilike', '%' . $query . '%')
                            ->get();

        return response()->json($obras);
    }
}