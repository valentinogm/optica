<?php

namespace App\Http\Controllers;

use App\Mail\EstadoPedidoActualizado;
use App\Mail\PedidoEntregado;
use App\Mail\PedidoNuevo;
use App\Models\ObraSocial;
use App\Models\Cliente;
use App\Models\Cristal;
use App\Models\Laboratorio;
use App\Models\LejosLente;
use App\Models\CercaLente;
use App\Models\Estado;
use App\Models\TipoArmazon;
use App\Models\Oftalmologo;
use App\Models\Estados_pedido;
use App\Models\Pedido;
use App\Models\TipoLente;
use App\Models\PedidoHistorialController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Dompdf\Dompdf;
use Dompdf\Options;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PedidosExport;
use Intervention\Image\Facades\Image;

class PedidoController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:pedido.index')->only('index');
        $this->middleware('can:pedido.create')->only('create','store');
        $this->middleware('can:pedido.edit')->only('edit','update');
        $this->middleware('can:pedido.destroy')->only('destroy');
        $this->middleware('can:pedido.clientesobtener')->only('obtenerClientes ');
        $this->middleware('can:pedido.verestados')->only('verEstados');
        $this->middleware('can:pedido.actualizarestado')->only('actualizarEstado');        
    }

    public function create()
    {
        $clientes = Cliente::orderBy('nombre')->orderBy('apellido')->get();
        $oftalmologos = Oftalmologo::orderBy('nombre')->orderBy('apellido')->get();
        $laboratorios = Laboratorio::orderBy('nombre')->get();
        $obras_sociales = ObraSocial::orderBy('nombre')->get();
        $tipo_armazon = TipoArmazon::all();
        $cristal = Cristal::all();
        $tipos_lente = TipoLente::all();
        return view('pedido.create', compact('obras_sociales', 'oftalmologos', 'laboratorios', 'clientes', 'tipo_armazon', 'cristal', 'tipos_lente'));
    }

    public function index(Request $request)
    {
        $pedidos = $this->getPedidos($request);
        // dd($request);
        return view('pedido.index', compact('pedidos'));
    }

    public function getPedidos(Request $request)
    {
        $query = $request->input('q');

        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        if ($startDate) {
            $startDate = date('Y-m-d', strtotime($startDate));
        }
        if ($endDate) {
            $endDate = date('Y-m-d', strtotime($endDate));
        }

        $pedidosQuery = Pedido::where(function ($queryBuilder) use ($query) {
            $queryBuilder->where('codigo', 'ilike', '%' . $query . '%')
                ->orWhereHas('cliente', function ($clienteQuery) use ($query) {
                    $clienteQuery->where('nombre', 'ilike', '%' . $query . '%')
                        ->orWhere('apellido', 'ilike', '%' . $query . '%')
                        ->orWhere('dni', 'ilike', $query . '%');
                });
        });

        // Filtros por fechas
        if ($startDate && $endDate) {
            $pedidosQuery->whereBetween('fecha_pedido', [$startDate, $endDate]);
        } elseif ($startDate) {
            $pedidosQuery->where('fecha_pedido', '>=', $startDate);
        } elseif ($endDate) {
            $pedidosQuery->where('fecha_pedido', '<=', $endDate);
        }

        // Ordenar por los pedidos próximos a vencer (fecha_estimada más cercana a la actual)
        $pedidos = $pedidosQuery
            // ->orderByRaw("(EXTRACT(EPOCH FROM (fecha_estimada - NOW())) / 86400) DESC") // Ordena por la fecha más próxima a vencer
            ->orderBy('fecha_pedido', 'desc') // Orden secundario por fecha de pedido
            ->orderBy('id', 'desc') // Orden secundario por ID
            ->paginate(15)
            ->appends(request()->query());
            // Explicación del cambio:

            // EXTRACT(EPOCH FROM (fecha_estimada - NOW())) / 86400:
            // EXTRACT(EPOCH FROM ...) obtiene el tiempo en segundos entre la fecha_estimada y la fecha actual (NOW()).
            // Luego dividimos por 86400 (la cantidad de segundos en un día) para convertirlo en días.
            // Al usar ASC, los pedidos con menos días de diferencia aparecerán primero (es decir, los que están más próximos a vencer).
        
        // Obtener el estado actual para cada pedido
        foreach ($pedidos as $pedido) {
            $estadoActual = DB::table('estados_pedido')
                ->where('id_pedido', $pedido->id)
                ->orderBy('created_at', 'desc')
                ->first();
            
            if ($estadoActual) {
                $estado = Estado::findOrFail($estadoActual->id_estado);
                $pedido->estadoActual = $estado->desc;
            } else {
                $pedido->estadoActual = 'Sin estado';
            }
        }
        // dd($pedidos);
        return $pedidos;
    }

    public function generatePdf(Request $request)
    {
        $pedidos = $this->getPedidos($request);
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        
        $dompdf = new Dompdf($options);
        $html = view('pedido.pdf', compact('pedidos'));
        $dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');
        
        // Render the HTML as PDF
        $dompdf->render();
        
        // Get the PDF content
        $pdfContent = $dompdf->output();
        
        // Send the PDF content as response with appropriate headers
        return response($pdfContent)
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'inline; filename="pedido.pdf"');
    }

    public function generateExcel(Request $request)
    {
        $pedidos = $this->getPedidos($request);
        $pedidosExcel = $pedidos->map(function ($pedido) {
            return [
                'codigo' => $pedido->codigo,
                'nombre_cliente' => $pedido->cliente->nombre . ' ' . $pedido->cliente->apellido,
                'dni_cliente' => $pedido->cliente->dni,
                'fecha_pedido' => $pedido->fecha_pedido,
                'fecha_entrega' => $pedido->fecha_entrega,
                'estado_actual' => $pedido->estadoActual,
                'ultima_edicion' => $pedido->updated_at,
                'Oftalmologo' => $pedido->oftalmologo->nombre . ' ' . $pedido->oftalmologo->apellido . ' - ' . $pedido->oftalmologo->matricula,
                'Laboratorio' => $pedido->laboratorio->nombre,
                'Obra Social' => $pedido->obraSocial->nombre,
            ];
        });
        
        return Excel::download(new PedidosExport($pedidosExcel), 'pedidos.xlsx');
    }

    public function store(Request $request)
    {
        // dd($request);
        try{
                $request->validate([
                    'cliente' => 'required',
                    'oftalmologo' => 'required',
                    'laboratorio' => 'required',
                    'obra_social' => 'required',
                    'importe' => 'required|numeric',
                    'senia' => 'required|numeric',
                    'receta' => 'image|mimes:jpeg,png,jpg,gif',
                    'receta_2' => 'image|mimes:jpeg,png,jpg,gif',
                ]);
                
                $cliente = Cliente::findOrFail($request->cliente);
                
                // Obtener nombres y apellidos del cliente
                $nombres = $cliente->nombre;
                $apellidos = $cliente->apellido;
                
                $pedido = new Pedido();
                $pedido->vendedor_id = Auth::id();
                $pedido->cliente_id = $request->cliente;
                $pedido->oftalmologo_id = $request->oftalmologo;
                $pedido->laboratorio_id = $request->laboratorio;
                $pedido->obra_social_id = $request->obra_social;
                $pedido->importe = $request->importe;
                $pedido->senia = $request->senia;
                $pedido->saldo = $request->importe - $request->senia;

                $pedido->fecha_pedido = date('Y-m-d');
                $fechaPedido = new \DateTime($pedido->fecha_pedido);

                
                if($request->has('tipo')){
                    $pedido->tipo = "Incluye Laboratorio";
                    $fechaEstimada = $fechaPedido->modify('+15 days');
                }else{
                    $pedido->tipo = "No Incluye laboratorio";
                    $fechaEstimada = $fechaPedido->modify('+7 days'); # lo que no son de laboratorio van a 7 dias de entrega
                }

                if($request->has('observaciones')){
                    $pedido->observaciones = $request->observaciones;
                }

                if ($request->hasFile('receta')) {
                    $image = $request->file('receta');
                    
                    // Redimensionar la imagen a 800x600
                    $resizedImage = Image::make($image)->orientate()->resize(800, 600);
                
                    // Crear un nombre único para la imagen
                    $imageName = time() . '.' . $image->getClientOriginalExtension();
                
                    // Guardar la imagen redimensionada en la carpeta 'recetas' del storage público
                    $resizedImage->save(public_path('storage/recetas/' . $imageName));
                
                    // Guardar la ruta de la imagen en el modelo $pedido
                    $pedido->receta = 'recetas/' . $imageName;
                }

                #Segunda carga de imagen de receta
                #En teoria armo y guardo la imagen 2
                if ($request->hasFile('receta_2')) {
                    $image2 = $request->file('receta_2');
                    
                    // Redimensionar la imagen a 800x600
                    $resizedImage2 = Image::make($image2)->orientate()->resize(800, 600);
                
                    // Crear un nombre único para la imagen
                    $imageName2 = time() . '.' . $image2->getClientOriginalExtension();
                
                    // Guardar la imagen redimensionada en la carpeta 'recetas' del storage público
                    $resizedImage2->save(public_path('storage/recetas_2/' . $imageName2));
                
                    // Guardar la ruta de la imagen en el modelo $pedido
                    $pedido->receta_2 = 'recetas_2/' . $imageName2;
                }

                $pedido->fecha_estimada = $fechaEstimada->format('Y-m-d');
                
                $pedido->save();
                $codigo = $this->generarCodigo($pedido->id, $nombres, $apellidos);
                $pedido->codigo = $codigo;

                // Guardar los detalles del lente de cerca si los campos están presentes y no son nulos
                if ($request->has('lente_cerca') && $request->lente_cerca['tipo_armazon'] !== null && $request->lente_cerca['cristal'] !== null && $request->lente_cerca['tipo_lente'] !== null) {
                    $lenteCerca = new CercaLente();
                    $lenteCerca->tipo_armazon_id = $request->lente_cerca['tipo_armazon'];
                    $lenteCerca->cristal_id = $request->lente_cerca['cristal'];
                    $lenteCerca->tipo_lente_id = $request->lente_cerca['tipo_lente'];
                    $lenteCerca->save();
                    $pedido->cerca_lente_id = $lenteCerca->id;
                }

                // Guardar los detalles del lente de lejos si los campos están presentes y no son nulos
                if ($request->has('lente_lejos') && $request->lente_lejos['tipo_armazon'] !== null && $request->lente_lejos['cristal'] !== null && $request->lente_lejos['tipo_lente'] !== null) {
                    $lenteLejos = new LejosLente();
                    $lenteLejos->tipo_armazon_id = $request->lente_lejos['tipo_armazon'];
                    $lenteLejos->cristal_id = $request->lente_lejos['cristal'];
                    $lenteLejos->tipo_lente_id = $request->lente_lejos['tipo_lente'];
                    $lenteLejos->save();
                    $pedido->lejos_lente_id = $lenteLejos->id;
                }
                // Guardar las referencias a los lentes en el pedido
                $pedido->save();

      
                $estado = new Estados_pedido();
                $estado->id_pedido = $pedido->id;
                $estado->id_estado = 1;
                $estado->save();
                if(!empty($pedido->cliente->mail)){
                Mail::to($pedido->cliente->mail)->send(new PedidoNuevo($pedido));
                }
            } catch (ValidationException $e) {
                return redirect()->back()->withErrors($e->validator->errors())->withInput();
            }
            return redirect()->route('pedido.created', ['id' => $pedido->id]);
    }
        
    public function created($id)
    {
       $pedido = Pedido::findOrFail($id);
       return view('pedido.pedidoCreado', compact('pedido'));
    }

    private function generarCodigo($id, $nombres, $apellidos)
    {
        $prefijo= 'PE';
        $iniciales = '';
        $nombres_array = explode(' ', $nombres);
        foreach ($nombres_array as $nombre) {
            $iniciales .= strtoupper(substr($nombre, 0, 1));
        }

        $apellidos_array = explode(' ', $apellidos);
        foreach ($apellidos_array as $apellido) {
            $iniciales .= strtoupper(substr($apellido, 0, 1));
        }

        $numero_id_formateado = str_pad($id, 3, '0', STR_PAD_LEFT);

        $codigo = $prefijo . $numero_id_formateado . $iniciales;

        return $codigo;
    }

    public function edit($id)
    {
        $Pedido = Pedido::findOrFail($id);
        $clientes = Cliente::all();
        $oftalmologos = Oftalmologo::all();
        $laboratorios = Laboratorio::all();
        $obras_sociales = ObraSocial::all();
        $tipo_armazon = TipoArmazon::all();
        $cristal = Cristal::all();
        $tipos_lente = TipoLente::all();
        return view('pedido.edit', compact('Pedido', 'obras_sociales', 'oftalmologos', 'laboratorios', 'clientes', 'tipo_armazon', 'cristal', 'tipos_lente'));
    }

    
    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'cliente' => 'required',
            'oftalmologo' => 'required',
            'laboratorio' => 'required',
            'obra_social' => 'required',
            'importe' => 'required|numeric',
            'senia' => 'required|numeric',
            'receta' => 'image|mimes:jpeg,png,jpg,gif',
            'receta_2' => 'image|mimes:jpeg,png,jpg,gif',
        ]);

        $pedido = Pedido::findOrFail($id);
    
        $pedidoHistorial = new PedidoHistorialController();
        $pedidoHistorial->fill($pedido->toArray());
        $pedidoHistorial->deleted_by_user_id = Auth::id();
        $pedidoHistorial->save();
        $pedido->cliente_id = $request->cliente;
        $pedido->oftalmologo_id = $request->oftalmologo;
        $pedido->laboratorio_id = $request->laboratorio;
        $pedido->obra_social_id = $request->obra_social;
        $pedido->importe = $request->importe;
        $pedido->senia = $request->senia;
        $pedido->saldo = $request->importe - $request->senia;
        $pedido->observaciones = $request->observaciones;

        if ($request->hasFile('receta')) {
            $image = $request->file('receta');
            
            // Redimensionar la imagen a 800x600
            $resizedImage = Image::make($image)->orientate()->resize(800, 600);
        
            // Crear un nombre único para la imagen
            $imageName = time() . '.' . $image->getClientOriginalExtension();
        
            // Guardar la imagen redimensionada en la carpeta 'recetas' del storage público
            $resizedImage->save(public_path('storage/recetas/' . $imageName));
        
            // Guardar la ruta de la imagen en el modelo $pedido
            $pedido->receta = 'recetas/' . $imageName;
        }
        
        if ($request->hasFile('receta_2')) {
            $image2 = $request->file('receta_2');
            
            // Redimensionar la imagen a 800x600
            $resizedImage2 = Image::make($image2)->orientate()->resize(800, 600);
        
            // Crear un nombre único para la imagen
            $imageName2 = time() . '.' . $image2->getClientOriginalExtension();
        
            // Guardar la imagen redimensionada en la carpeta 'recetas' del storage público
            $resizedImage2->save(public_path('storage/recetas_2/' . $imageName2));
        
            // Guardar la ruta de la imagen en el modelo $pedido
            $pedido->receta_2 = 'recetas_2/' . $imageName2;
        }

        if ($request->has('lente_cerca')) {
            $lenteCerca = $request->lente_cerca;
            $tipoArmazon = $lenteCerca['tipo_armazon'] ?? null;
            $cristal = $lenteCerca['cristal'] ?? null;
            $tipoLente = $lenteCerca['tipo_lente'] ?? null;
        
            $todosNulos = is_null($tipoArmazon) && is_null($cristal) && is_null($tipoLente);
            $todosNoNulos = !is_null($tipoArmazon) && !is_null($cristal) && !is_null($tipoLente);
        
            if ($todosNulos) {
                // Eliminar lente de cerca si todos los campos son null
                if ($pedido->cerca_lente_id) {
                    $pedido->CercaLente->delete();
                    $pedido->cerca_lente_id = null;
                }
            } elseif ($todosNoNulos) {
                $cercaLente = $pedido->CercaLente;
                $diferente = !$cercaLente || 
                             $cercaLente->tipo_armazon_id != $tipoArmazon || 
                             $cercaLente->cristal_id != $cristal || 
                             $cercaLente->tipo_lente_id != $tipoLente;
        
                if ($diferente) {
                    $nuevoLenteCerca = new CercaLente();
                    $nuevoLenteCerca->tipo_armazon_id = $tipoArmazon;
                    $nuevoLenteCerca->cristal_id = $cristal;
                    $nuevoLenteCerca->tipo_lente_id = $tipoLente;
                    $nuevoLenteCerca->save();
                    $pedido->cerca_lente_id = $nuevoLenteCerca->id;
                }
            }
        }

        if ($request->has('lente_lejos')) {
            $lenteLejos = $request->lente_lejos;
            $tipoArmazon = $lenteLejos['tipo_armazon'] ?? null;
            $cristal = $lenteLejos['cristal'] ?? null;
            $tipoLente = $lenteLejos['tipo_lente'] ?? null;
        
            $todosNulos = is_null($tipoArmazon) && is_null($cristal) && is_null($tipoLente);
            $todosNoNulos = !is_null($tipoArmazon) && !is_null($cristal) && !is_null($tipoLente);
        
            if ($todosNulos) {
                // Eliminar lente de cerca si todos los campos son null
                if ($pedido->lejos_lente_id) {
                    $pedido->LejosLente->delete();
                    $pedido->lejos_lente_id = null;
                }
            } elseif ($todosNoNulos) {
                $lejosLente = $pedido->LejosLente;
                $diferente = !$lejosLente || 
                             $lejosLente->tipo_armazon_id != $tipoArmazon || 
                             $lejosLente->cristal_id != $cristal || 
                             $lejosLente->tipo_lente_id != $tipoLente;
        
                if ($diferente) {
                    $nuevoLenteLejos = new LejosLente();
                    $nuevoLenteLejos->tipo_armazon_id = $tipoArmazon;
                    $nuevoLenteLejos->cristal_id = $cristal;
                    $nuevoLenteLejos->tipo_lente_id = $tipoLente;
                    $nuevoLenteLejos->save();
                    $pedido->lejos_lente_id = $nuevoLenteLejos->id;
                }
            }
        }
        $pedido->save();
        return redirect()->route('pedido.index')->with('success', 'ok');
    }

    public function destroy($id)
    {
        $Pedido = Pedido::findOrFail($id);
        $Pedido->deleted_by_user_id = Auth::id();
        $Pedido->save();
        $Pedido->delete();
        return redirect()->route('pedido.index')->with('eliminar','ok');    
    }
    
    public function show($id)
    {
        $pedido = Pedido::findOrFail($id);
        $tipo_armazon = TipoArmazon::all();
        return view('pedido.show', compact('pedido','tipo_armazon'));
    }

    public function obtenerClientes(Request $request) : JsonResponse
    {
        $query = $request->input('q');
        $clientes = Cliente::where('nombre', 'ilike', '%' . $query . '%')
                            ->orWhere('apellido', 'ilike', '%' . $query . '%')
                            ->orWhere('dni', 'ilike', '%' . $query . '%')
                            ->get();

        return response()->json($clientes);
    }

    public function verEstados($id)
    {
        $pedido = Pedido::findOrFail($id);
        $estados = Estados_pedido::where('id_pedido', $pedido->id)
                         ->orderBy('created_at', 'asc')
                          ->get();
        $estadoActual = $estados->last(); 
        // foreach ($estados as $estado){
        //     $estadoActual = Estado::find($estado->id_estado);
        //     echo $estadoActual->desc;
        //     echo "<br>";
        // }
        $siguienteEstado = Estado::find($estadoActual->id_estado + 1);
        return view('pedido.verEstados', compact('pedido', 'estados', 'estadoActual', 'siguienteEstado'));
    }
    
    public function actualizarEstado($id_pedido, $id_estado)
    {
        $pedido = Pedido::findOrFail($id_pedido);
        $estado = new Estados_pedido();
        $estado->id_pedido = $pedido->id;
        $estado->id_estado = $id_estado;
        $estado->save();
        $estadoactual = Estado::findOrFail($id_estado);
        $estadodesc = $estadoactual->desc;

        if ($id_estado == 4) {
            $pedido->fecha_entrega = now();
            $pedido->save();
            if(!empty($pedido->cliente->mail)){
                Mail::to($pedido->cliente->mail)->send(new PedidoEntregado($pedido));
            }
        }else{
            if(!empty($pedido->cliente->mail)){
            Mail::to($pedido->cliente->mail)->send(new EstadoPedidoActualizado($pedido, $estado, $estadodesc));
            }
        }
        return redirect()->route('pedido.index')->with('success', 'ok');
    }

    public function lab($id)
    {
        $pedido = Pedido::findOrFail($id);
        return view('pedido.showLaboratorio', compact('pedido'));
    }
}