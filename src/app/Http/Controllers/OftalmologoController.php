<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Oftalmologo;
use Illuminate\Http\JsonResponse;

class OftalmologoController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Oftalmologo.index')->only('index');
        $this->middleware('can:Oftalmologo.create')->only('create','store');
        $this->middleware('can:Oftalmologo.edit')->only('edit','update');
        $this->middleware('can:Oftalmologo.destroy')->only('destroy');
    }
    public function index(Request $request)
    {
        $query = $request->input('q');
        $oftalmologos = Oftalmologo::where('nombre', 'ilike', '%' . $query . '%')
                                    ->orWhere('apellido', 'ilike', '%' . $query . '%')
                                    ->orWhere('matricula', 'ilike', '%' . $query . '%')
                                    ->paginate(10);
        return view('Oftalmologos.index', ['oftalmologos' => $oftalmologos]);
    }

    public function destroy(Oftalmologo $id)
    {
        $oftalmologo = Oftalmologo::findOrFail($id->id);
        $user_id = Auth::id();
        $oftalmologo->deleted_by_user_id = $user_id;
        $oftalmologo->save();
        $oftalmologo->delete();
        return redirect()->route('Oftalmologo.index')->with('eliminar', 'ok');
    }

    public function create()
    {
        return view('Oftalmologos.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'matricula' => 'required|string|max:255|unique:oftalmologo',
        ]);

        $oftalmologo = new Oftalmologo();
        $oftalmologo->nombre = $request->nombre;
        $oftalmologo->apellido = $request->apellido;
        $oftalmologo->matricula = $request->matricula;
        $oftalmologo->save();
        return redirect()->route('Oftalmologo.index')->with('success', 'ok');
    }

    public function edit($id)
    {
        $oftalmologo = Oftalmologo::findOrFail($id);
        return view('Oftalmologos.edit', compact('oftalmologo'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'matricula' => 'required|string|max:255|unique:oftalmologo,matricula,' . $id,
        ]);

        $oftalmologo = Oftalmologo::findOrFail($id);
        $oftalmologo->nombre = $request->nombre;
        $oftalmologo->apellido = $request->apellido;
        $oftalmologo->matricula = $request->matricula;
        $oftalmologo->save();

        return redirect()->route('Oftalmologo.index')->with('success', 'ok');
    }

    public function obtenerOftalmologos(Request $request) : JsonResponse
    {
        $query = $request->input('q');
        $oftalmologos = Oftalmologo::where('nombre', 'ilike', '%' . $query . '%')
                            ->orWhere('apellido', 'ilike', '%' . $query . '%')
                            ->orWhere('matricula', 'ilike', '%' . $query . '%')
                            ->get();

        return response()->json($oftalmologos);
    }
}
