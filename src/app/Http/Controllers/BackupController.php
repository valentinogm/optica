<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;


class BackupController extends Controller
{
    public function index()
    {
        $files = Storage::disk('backups')->files('optica');

        $backups = [];
        foreach ($files as $file) {
            if (substr($file, -4) == '.zip' && Storage::disk('backups')->exists($file)) {
                $backups[] = [
                    'file_path' => $file,
                    'file_name' => str_replace('optica/', '', $file),
                    'file_size' => Storage::disk('backups')->size($file),
                    'last_modified' => Carbon::createFromTimestamp(Storage::disk('backups')->lastModified($file))->toDateTimeString(),
                ];
            }
        }

        return view('backups.index', compact('backups'));
    }

    public function create()
    {
        try {
            Artisan::call('backup:run');
        } catch ( \Exception $e ){
            return redirect()->route('backups.index')
                ->with('error', 'An error occurred: ' . $e->getMessage());
        }
        return redirect()->route('backups.index')
            ->with('success', 'Run backup succesfully');
    }

    public function download($file_name)
    {
        $file = 'optica/' . $file_name;

        if (Storage::disk('backups')->exists($file)) {
            return response()->download(storage_path('app/backup/' . $file));
        } else {
            abort(404, "El archivo de backup no existe.");
        }
    }

    public function delete($file_name)
    {
        $file = 'optica/' . $file_name;

        if (Storage::disk('backups')->exists($file)) {
            Storage::disk('backups')->delete($file);
            return back()->with('success', 'Backup eliminado exitosamente.');
        } else {
            abort(404, "El archivo de backup no existe.");
        }
    }

    public function cleanBackup()
    {
        try {
            // Ejecuta el comando de Artisan
            Artisan::call('backup:clean');

            // Obtén la salida del comando
            $output = Artisan::output();

            // Registra la salida en el log
            Log::info('Backup clean command executed successfully.', ['output' => $output]);

            // Redirige de vuelta con un mensaje de éxito
            return redirect()->back()->with('success', 'Backup clean command executed successfully.');
        } catch (\Exception $e) {
            // En caso de error, registra el error en el log
            Log::error('Failed to execute backup clean command.', ['error' => $e->getMessage()]);

            // Redirige de vuelta con un mensaje de error
            return redirect()->back()->with('error', 'Failed to execute backup clean command: ' . $e->getMessage());
        }
    }
}
