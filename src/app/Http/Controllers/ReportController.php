<?php

namespace App\Http\Controllers;

use App\Exports\PedidosExport;
use App\Http\Controllers\Controller;
use App\Models\Pedido;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;

class ReportController extends Controller
{
    public function index()
    {
        return view('report.index');
    }

    public function generateReport(Request $request)
    {
        $filters = $request->all();
        $desde = $request->date_to;
        $hasta = $request->date_from;

        // Ejemplo de consulta con filtros
        $query = Pedido::query()->with(['cliente', 'oftalmologo', 'obraSocial', 'cercalente.tipoLente', 'lejoslente.tipoLente']);

        if (!empty($filters['date_from']) && !empty($filters['date_to'])) {
            $query->whereBetween('fecha_pedido', [$filters['date_from'], $filters['date_to']]);
        } elseif (!empty($filters['date_from'])) {
            $query->where('fecha_pedido', '>=', $filters['date_from']);
        } elseif (!empty($filters['date_to'])) {
            $query->where('fecha_pedido', '<=', $filters['date_to']);
        }

        $pedidos = $query->get();

        /**
         * Mapeo de datos para la exportación a Excel
         * Campos requeridos:
         * - Cliente.
         * - Oftalmologo.
         * - Codigo.
         * - Obra social.
         * - Lentes (No tipo de armazon).
         * - Importe completo.
         * - Fecha del Pedido.
         */

        $pedidosExcel = $pedidos->map(function ($pedido) {
            return [
                'nombre_cliente' => $pedido->cliente->nombre . ' ' . $pedido->cliente->apellido,
                'Oftalmologo' => $pedido->oftalmologo->nombre . ' ' . $pedido->oftalmologo->apellido . ' - ' . $pedido->oftalmologo->matricula,
                'codigo' => $pedido->codigo,
                'Obra Social' => $pedido->obraSocial->nombre,
                'cerca_lente' => optional(optional($pedido->cercalente)->tipoLente)->nombre ?? 'N/A',
                'lejos_lente' => optional(optional($pedido->lejoslente)->tipoLente)->nombre ?? 'N/A',
                'fecha_pedido' => \Carbon\Carbon::parse($pedido->fecha_pedido)->format('d/m/Y'), // Aseguramos que sea una instancia de fecha antes de formatear
                'laboratorio' => $pedido->laboratorio->nombre,
                'importe' => $pedido->importe,
                'senia' => $pedido->senia,
                'deuda' => $pedido->saldo,
            ];
        });

        return Excel::download(new PedidosExport($pedidosExcel), 'pedidos.xlsx');

    }

    public function getInformacionUsuarios(Request $request)
    {
        $userCount = User::count();
        return response()->json(['user_count' => $userCount]);
    }
    public function getInformacionRoles(Request $request)
    {
        $roleCount = Role::count();
        return response()->json(['role_count' => $roleCount]);
    }

}
