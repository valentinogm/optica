<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class VendedorController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:vendedor.index')->only('index');
        $this->middleware('can:vendedor.create')->only('create','store');
        $this->middleware('can:vendedor.edit')->only('edit','update');
        $this->middleware('can:vendedor.destroy')->only('destroy');
    }

    public function index(Request $request)
    {
        $query = $request->input('q');

        $vendedores = User::where('name', 'ilike', '%' . $query . '%')
                          ->orWhere('apellido', 'ilike', '%' . $query . '%')
                          ->orWhere('email', 'ilike', '%' . $query . '%')
                          ->paginate(10);
        return view('vendedor.index', compact('vendedores'));
    }
    

    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        return view('vendedor.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'documento' => 'required|min:5|max:12|unique:users,documento',
            'role' => 'required|string|exists:roles,name',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->documento = $request->documento;
        $user->apellido = $request->apellido;
        $user->password = Hash::make($request->password);
        $user->save();

        $user->assignRole($request->role);

        return redirect()->route('vendedor.index')->with('success', 'ok');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::pluck('name', 'name')->all();
        return view('vendedor.edit', compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $id,
            'role' => 'required|string|exists:roles,name',
        ]);

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->apellido = $request->apellido;
        $user->email = $request->email;
        $user->save();

        $user->syncRoles([$request->role]);

        return redirect()->route('vendedor.index')->with('success', 'ok');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('vendedor.index')->with('eliminar', 'ok');
    }
}
