<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Laboratorio;
use Illuminate\Http\JsonResponse;

class LaboratorioController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Laboratorio.index')->only('index');
        $this->middleware('can:Laboratorio.create')->only('create','store');
        $this->middleware('can:Laboratorio.edit')->only('edit','update');
        $this->middleware('can:Laboratorio.destroy')->only('destroy');
    }
    public function index(Request $request)
    {
        $query = $request->input('q');
        $laboratorios = Laboratorio::where('nombre', 'ilike', '%' . $query . '%')
                                    ->orWhere('nro', 'ilike', '%' . $query . '%')
                                    ->orderBy('nombre','asc')
                                    ->paginate(10);
        return view('Laboratorios.index', ['laboratorios' => $laboratorios]);
    }

    public function destroy(Laboratorio $id)
    {
        $laboratorio = Laboratorio::findOrFail($id->id);
        $user_id = Auth::id();
        $laboratorio->deleted_by_user_id = $user_id;
        $laboratorio->save();
        $laboratorio->delete();
        return redirect()->route('Laboratorios.index')->with('eliminar', 'ok');
    }

    public function create()
    {
        return view('Laboratorios.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'nro' => 'required|string|max:255|unique:laboratorio',
        ]);
        
        $laboratorio = new Laboratorio();
        $laboratorio->nombre = $request->nombre;
        $laboratorio->nro = $request->nro;
        $laboratorio->save();
        return redirect()->route('Laboratorio.index')->with('success', 'ok');

    }

    public function edit($id)
    {
        $laboratorio = Laboratorio::findOrFail($id);
        return view('Laboratorios.edit', compact('laboratorio'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'nro' => 'required|string|max:255|unique:laboratorio,nro,' . $id,
        ]);

        $laboratorio = Laboratorio::findOrFail($id);
        $laboratorio->nombre = $request->nombre;
        $laboratorio->nro = $request->nro;
        $laboratorio->save();

        return redirect()->route('Laboratorio.index')->with('success', 'ok');
    }

    public function obtenerOLaboratorios(Request $request) : JsonResponse
    {
        $query = $request->input('q');
        $obras = Laboratorio::where('nombre', 'ilike', '%' . $query . '%')
                            ->orWhere('nro', 'ilike', '%' . $query . '%')
                            ->get();

        return response()->json($obras);
    }
}
