<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Cristal;

class CristalController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:cristal.index')->only('index');
        $this->middleware('can:cristal.create')->only('create','store');
        $this->middleware('can:cristal.edit')->only('edit','update');
        $this->middleware('can:cristal.destroy')->only('destroy');
    }
    public function index(Request $request)
    {
        $query = $request->input('q');
        $cristales = Cristal::where('nombre', 'ilike', '%' . $query . '%')
                            ->paginate(10);
        return view('cristal.index', ['cristales' => $cristales]);
    }

    public function destroy(Cristal $id)
    {
        $cristal = Cristal::findOrFail($id->id);
        $user_id = Auth::id();
        $cristal->deleted_by_user_id = $user_id;
        $cristal->save();
        $cristal->delete();
        return redirect()->route('cristal.index')->with('eliminar', 'ok');
    }

    public function create()
    {
        return view('cristal.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255|unique:cristal',
        ]);
        
        $cristal = new Cristal();
        $cristal->nombre = $request->nombre;
        $cristal->save();
        return redirect()->route('cristal.index')->with('success', 'ok');

    }

    public function edit($id)
    {
        $cristal = Cristal::findOrFail($id);
        return view('cristal.edit', compact('cristal'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string|max:255|unique:cristal,nombre,' . $id,
        ]);

        $cristal = Cristal::findOrFail($id);
        $cristal->nombre = $request->nombre;
        $cristal->save();

        return redirect()->route('cristal.index')->with('success', 'ok');
    }
}
