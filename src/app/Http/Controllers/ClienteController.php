<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Cliente;
use Illuminate\Validation\ValidationException;

class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:Clientes.index')->only('index');
        $this->middleware('can:Clientes.create')->only('create','store');
        $this->middleware('can:Clientes.edit')->only('edit','update');
        $this->middleware('can:Clientes.destroy')->only('destroy');
    }
    
    public function index(Request $request)
    {
        $query = $request->input('q');
        $clientes = Cliente::where('nombre', 'ilike', '%' . $query . '%')
                            ->orWhere('apellido', 'ilike', '%' . $query . '%')
                            ->orWhere('dni', 'ilike', '%' . $query . '%')
                            ->orderBy('nombre','asc')
                            ->orderBy('apellido','asc')
                            ->paginate(10);
        return view('Clientes.index', ['clientes' => $clientes]);
    }

    public function destroy(Cliente $id)
    {
        $cliente = Cliente::findOrFail($id->id);
        $user_id = Auth::id();
        $cliente->deleted_by_user_id = $user_id;
        $cliente->save();
        $cliente->delete();
        return redirect()->route('Clientes.index')->with('eliminar', 'ok');
    }

    public function create()
    {
        return view('Clientes.create');
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'nombre' => 'required|string|max:255',
                'apellido' => 'required|string|max:255',
                'dni' => 'required|string|max:10|unique:cliente',
                'domicilio' => 'required|string|max:255',
                'localidad' => 'required|string|max:255',
                'fecha_nac' => 'required|date',
                'telefono' => 'nullable|string|max:20',
                'mail' => 'nullable|string|email|unique:cliente,mail',
            ]);

            $cliente = new Cliente();
            $cliente->nombre = $request->nombre;
            $cliente->apellido = $request->apellido;
            $cliente->dni = $request->dni;
            $cliente->domicilio = $request->domicilio;
            $cliente->localidad = $request->localidad;
            $cliente->fecha_nac = $request->fecha_nac;
            $cliente->telefono = $request->telefono;
            if ($request->has('mail')) {
                $cliente->mail = $request->mail;
            }
            $cliente->save();

            return redirect()->route('Clientes.index')->with('success', 'ok');
        } catch (ValidationException $e) {
            return redirect()->back()->withErrors($e->validator->errors())->withInput();
        }
    }


    public function edit($id)
    {
        $cliente = Cliente::findOrFail($id);
        return view('Clientes.edit', compact('cliente'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'dni' => 'required|string|max:10|unique:cliente,dni,' . $id,
            'domicilio' => 'required|string|max:255',
            'localidad' => 'required|string|max:255',
            'fecha_nac' => 'required|date',
            'telefono' => 'required|string|max:20',
            'mail' => 'nullable|email|max:255',
        ]);

        $cliente = Cliente::findOrFail($id);
        $cliente->nombre = $request->nombre;
        $cliente->apellido = $request->apellido;
        $cliente->dni = $request->dni;
        $cliente->domicilio = $request->domicilio;
        $cliente->localidad = $request->localidad;
        $cliente->fecha_nac = $request->fecha_nac;
        $cliente->telefono = $request->telefono;
        $cliente->mail = $request->mail;
        $cliente->save();

        return redirect()->route('Clientes.index')->with('success', 'ok');
    }

    public function show($id)
    {
        $cliente = Cliente::findOrFail($id);
        return view('Clientes.show', compact('cliente'));
    }
}
