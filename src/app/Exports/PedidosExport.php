<?php

namespace App\Exports;

use App\Models\Pedido;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PedidosExport implements FromCollection, WithHeadings
{
    protected $pedidos;

    public function __construct($pedidos)
    {
        $this->pedidos = $pedidos;
    }

    public function collection()
    {
        return $this->pedidos;
    }

    public function headings(): array
    {
        return [            
            'Cliente',
            'Oftalmologo',
            'Codigo',
            'Obra Social',
            'Lente Cerca',
            'Lente lejos',
            'Fecha pedido',
            'Laboratorio',
            'Importe',
            'Senia',
            'Deuda',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FFORMAT_TEXT, // Cliente
            'B' => NumberFormat::FFORMAT_TEXT, // Oftalmologo
            'C' => NumberFormat::FFORMAT_TEXT, // Codigo
            'D' => NumberFormat::FORMAT_TEXT, // Obra social
            'E' => NumberFormat::FORMAT_TEXT, // Lente cerca
            'F' => NumberFormat::FORMAT_TEXT, // Lente lejos
            'G' => NumberFormat::FORMAT_DATE_DDMMYYYY, // Fecha pedido
        ];
    }
}
