<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CercaLente extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'cerca_lente';
    protected $dates = ['deleted_at'];

    public function tipo_armazon()
    {
        return $this->belongsTo(TipoArmazon::class, 'tipo_armazon_id');
    }
    
    public function cristal()
    {
        return $this->belongsTo(Cristal::class, 'cristal_id');
    }

    public function tipoLente()
    {
        return $this->belongsTo(TipoLente::class);
    }
}
