<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cristal extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'cristal';
    protected $dates = ['deleted_at'];
    
}
