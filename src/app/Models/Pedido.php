<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Pedido extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'pedido';
    protected $dates = ['deleted_at'];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function vendedor()
    {
        return $this->belongsTo(User::class, 'vendedor_id');
    }

    public function oftalmologo()
    {
        return $this->belongsTo(Oftalmologo::class);
    }

    public function laboratorio()
    {
        return $this->belongsTo(Laboratorio::class);
    }

    public function cercaLente()
    {
        return $this->belongsTo(CercaLente::class);
    }

    public function lejosLente()
    {
        return $this->belongsTo(LejosLente::class);
    }

    public function obraSocial()
    {
        return $this->belongsTo(ObraSocial::class);
    }
}
