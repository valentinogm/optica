<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObraSocial extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'obra_social';
    protected $dates = ['deleted_at'];

}
