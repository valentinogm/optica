<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PedidoHistorialController extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'pedido_historial';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'codigo',
        'vendedor_id',
        'cliente_id',
        'oftalmologo_id',
        'laboratorio_id',
        'cerca_lente_id',
        'lejos_lente_id',
        'obra_social_id',
        'fecha_pedido',
        'fecha_estimada',
        'fecha_entrega',
        'importe',
        'senia',
        'saldo',
        'receta',
        'deleted_by_user_id',
    ];
    

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function vendedor()
    {
        return $this->belongsTo(User::class, 'vendedor_id');
    }

    public function oftalmologo()
    {
        return $this->belongsTo(Oftalmologo::class);
    }

    public function laboratorio()
    {
        return $this->belongsTo(Laboratorio::class);
    }

    public function cercaLente()
    {
        return $this->belongsTo(CercaLente::class);
    }

    public function lejosLente()
    {
        return $this->belongsTo(LejosLente::class);
    }

    public function obraSocial()
    {
        return $this->belongsTo(ObraSocial::class);
    }
}
